package main

import (
	"fmt"
	"strings"
	"sync"
	"unicode"
	"unicode/utf8"
)

type stateFn func(*Lexer) stateFn
type ItemType int

type Item struct {
	typ  ItemType
	val  string
	line int
	col  int
}

const (
	eof      = rune(-1)
	digits   = "0123456789"
	idValids = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
)

const (
	ItemError ItemType = iota
	ItemBlock
	ItemDot
	ItemEOF
	ItemConst
	ItemVar
	ItemIdentifier
	ItemComma
	ItemSemicolon
	ItemEqual
	ItemNumber
	ItemProcedure
	ItemAssign
	ItemMinus
	ItemPlus
	ItemTimes
	ItemDivide
	ItemOpenParen
	ItemCloseParen
	ItemCall
	ItemBegin
	ItemEnd
	ItemIf
	ItemThen
	ItemGreaterThan
	ItemLessThan
	ItemDifferent
	ItemGreaterThanOrEqual
	ItemLessThanOrEqual
	ItemOdd
	ItemWhile
	ItemDo
	ItemReadln
	ItemWriteln
	ItemWrite
	ItemString
	ItemInc
)

type Lexer struct {
	name  string
	input string
	start int
	pos   int
	width int
	line  int
	col   int
	items chan Item
}

var table = map[string]ItemType{
	"const":     ItemConst,
	"var":       ItemVar,
	"procedure": ItemProcedure,
	"call":      ItemCall,
	"begin":     ItemBegin,
	"end":       ItemEnd,
	"if":        ItemIf,
	"then":      ItemThen,
	"odd":       ItemOdd,
	"while":     ItemWhile,
	"do":        ItemDo,
	"readln":    ItemReadln,
	"writeln":   ItemWriteln,
	"write":     ItemWrite,
	"inc":       ItemInc,
}

func (i Item) String() string {
	switch i.typ {
	case ItemEOF:
		return "EOF"
	case ItemError:
		return i.val
	case ItemConst:
		return "CONST:\t\t" + i.val
	case ItemVar:
		return "VAR:\t\t" + i.val
	case ItemIdentifier:
		return "IDENTIFIER:\t" + i.val
	case ItemComma:
		return "COMMA:\t\t" + i.val
	case ItemSemicolon:
		return "SEMICOLON:\t" + i.val
	case ItemEqual:
		return "EQUAL:\t\t" + i.val
	case ItemNumber:
		return "NUMBER:\t\t" + i.val
	case ItemProcedure:
		return "PROCEDURE:\t" + i.val
	case ItemAssign:
		return "ASSIGN:\t\t" + i.val
	case ItemMinus:
		return "MINUS:\t\t" + i.val
	case ItemPlus:
		return "PLUS:\t\t" + i.val
	case ItemTimes:
		return "TIMES:\t\t" + i.val
	case ItemDivide:
		return "DIVIDE:\t\t" + i.val
	case ItemOpenParen:
		return "OPEN_PAREN:\t" + i.val
	case ItemCloseParen:
		return "CLOSE_PAREN:\t" + i.val
	case ItemBegin:
		return "BEGIN:\t\t" + i.val
	case ItemCall:
		return "CALL:\t\t" + i.val
	case ItemEnd:
		return "END:\t\t" + i.val
	case ItemDot:
		return "DOT:\t\t" + i.val
	case ItemIf:
		return "IF:\t\t" + i.val
	case ItemThen:
		return "THEN:\t\t" + i.val
	case ItemGreaterThan:
		return "GREATER_THAN:\t" + i.val
	case ItemGreaterThanOrEqual:
		return "GR_OR_EQ:\t" + i.val
	case ItemLessThan:
		return "LESS_THAN:\t" + i.val
	case ItemLessThanOrEqual:
		return "LE_OR_EQ:\t" + i.val
	case ItemWriteln:
		return "WRITELN:\t" + i.val
	case ItemWrite:
		return "WRITE:\t\t" + i.val
	case ItemReadln:
		return "READLN:\t\t" + i.val
	case ItemString:
		return "STRING:\t\t" + i.val
	case ItemWhile:
		return "WHILE:\t\t" + i.val
	case ItemDifferent:
		return "DIFFERENT:\t" + i.val
	case ItemDo:
		return "DO:\t\t" + i.val
	}

	if len(i.val) > 10 {
		return fmt.Sprintf("%.10q...", i.val)
	}

	return fmt.Sprintf("%q", i.val)
}

func Lex(name, input string, wg *sync.WaitGroup) *Lexer {
	l := &Lexer{
		name:  name,
		input: input,
		line:  1,
		col:   0,
		items: make(chan Item),
	}

	go l.run(wg)
	return l
}

func (l *Lexer) run(wg *sync.WaitGroup) {
	defer wg.Done()
	for state := lexBlock; state != nil; {
		state = state(l)
	}
	close(l.items)
}

func (l *Lexer) emit(t ItemType) {
	l.items <- Item{
		t,
		l.input[l.start:l.pos],
		l.line,
		l.col - (l.pos - l.start),
	}
	l.start = l.pos
}

func (l *Lexer) emitString(t ItemType) {
	l.items <- Item{
		t,
		l.input[l.start+1 : l.pos-1],
		l.line,
		l.col - (l.pos - l.start),
	}
	l.start = l.pos
}

func (l *Lexer) next() (r rune) {
	l.col++

	r, l.width = utf8.DecodeRuneInString(l.input[l.pos:])
	l.pos += l.width

	return r
}

func (l *Lexer) ignore() {
	l.start = l.pos
}

func (l *Lexer) backup() {
	l.pos -= l.width
	l.col -= l.width

	// if l.width == 1 && l.input[l.pos] == '\n' {
	// 	l.line--
	// }
}

func (l *Lexer) peek() rune {
	r := l.next()
	l.backup()
	return r
}

func (l *Lexer) acceptRun(valid string) {
	for strings.ContainsRune(valid, l.next()) {
	}
	l.backup()
}

// lex states

func lexBlock(l *Lexer) stateFn {
	for {
		switch r := l.next(); {

		case unicode.IsLetter(r):
			l.acceptRun(idValids)
			if table[strings.ToLower(l.input[l.start:l.pos])] > 0 {
				l.emit(table[strings.ToLower(l.input[l.start:l.pos])])

			} else {
				l.emit(ItemIdentifier)
			}

		case unicode.IsDigit(r):
			l.acceptRun(digits)
			l.emit(ItemNumber)

		case isSpace(r):
			l.ignore()

		case isEndOfLine(r):
			l.line++
			l.col = 0
			l.ignore()

		case r == ';':
			l.emit(ItemSemicolon)

		case r == ':':
			l.pos++
			l.col++
			l.emit(ItemAssign)

		case r == ',':
			l.emit(ItemComma)

		case r == '-':
			if unicode.IsDigit(l.peek()) {
				l.acceptRun(digits)
				l.emit(ItemNumber)

			} else {
				l.emit(ItemMinus)
			}

		case r == '+':
			l.emit(ItemPlus)

		case r == '*':
			l.emit(ItemTimes)

		case r == '/':
			l.emit(ItemDivide)

		case r == '(':
			l.emit(ItemOpenParen)

		case r == ')':
			l.emit(ItemCloseParen)

		case r == '>':
			if l.peek() == '=' {
				l.pos++
				l.col++
				l.emit(ItemGreaterThanOrEqual)

			} else {
				l.emit(ItemGreaterThan)
			}

		case r == '<':
			if l.peek() == '>' {
				l.pos++
				l.col++
				l.emit(ItemDifferent)

			} else if l.peek() == '=' {
				l.pos++
				l.col++
				l.emit(ItemLessThanOrEqual)

			} else {
				l.emit(ItemLessThan)
			}

		case r == '=':
			l.emit(ItemEqual)

		case r == '"':
			for {
				if l.next() == '"' {
					l.emitString(ItemString)
					break
				}
			}

		case r == '\'':
			for {
				if l.next() == '\'' {
					l.emitString(ItemString)
					break
				}
			}

		case r == '.':
			l.emit(ItemDot)

		case r == eof:
			l.emit(ItemEOF)
			return nil
		}

		if l.pos >= len(l.input) {
			l.width = 0
			return nil
		}
	}
}

// helper functions

func isSpace(r rune) bool {
	return r == ' ' || r == '\t'
}

func isEndOfLine(r rune) bool {
	return r == '\n'
}
