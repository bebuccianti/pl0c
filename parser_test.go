package main

import (
	"io/ioutil"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseBien00(t *testing.T) {
	buf, err := ioutil.ReadFile("test/parser/BIEN-00.PL0")
	if err != nil {
		t.Errorf("Error open file test/parser/BIEN-00.PL0")
	}

	var wg sync.WaitGroup
	wg.Add(2)

	lexer := Lex("BIEN-00.PL0", string(buf), &wg)
	parser := Parse(lexer, &wg)

	wg.Wait()

	assert.Empty(t, parser.err)
}

func TestParseBien01(t *testing.T) {
	buf, err := ioutil.ReadFile("test/parser/BIEN-01.PL0")
	if err != nil {
		t.Errorf("Error open file test/parser/BIEN-01.PL0")
	}

	var wg sync.WaitGroup
	wg.Add(2)

	lexer := Lex("BIEN-01.PL0", string(buf), &wg)
	parser := Parse(lexer, &wg)

	wg.Wait()

	assert.Empty(t, parser.err)
}

func TestParseBien02(t *testing.T) {
	buf, err := ioutil.ReadFile("test/parser/BIEN-02.PL0")
	if err != nil {
		t.Errorf("Error open file test/parser/BIEN-02.PL0")
	}

	var wg sync.WaitGroup
	wg.Add(2)

	lexer := Lex("BIEN-02.PL0", string(buf), &wg)
	parser := Parse(lexer, &wg)

	wg.Wait()

	assert.Empty(t, parser.err)
}

func TestParseBien03(t *testing.T) {
	buf, err := ioutil.ReadFile("test/parser/BIEN-03.PL0")
	if err != nil {
		t.Errorf("Error open file test/parser/BIEN-03.PL0")
	}

	var wg sync.WaitGroup
	wg.Add(2)

	lexer := Lex("BIEN-03.PL0", string(buf), &wg)
	parser := Parse(lexer, &wg)

	wg.Wait()

	assert.Empty(t, parser.err)
}

func TestParseBien04(t *testing.T) {
	buf, err := ioutil.ReadFile("test/parser/BIEN-04.PL0")
	if err != nil {
		t.Errorf("Error open file test/parser/BIEN-04.PL0")
	}

	var wg sync.WaitGroup
	wg.Add(2)

	lexer := Lex("BIEN-04.PL0", string(buf), &wg)
	parser := Parse(lexer, &wg)

	wg.Wait()

	assert.Empty(t, parser.err)
}

func TestParseBien05(t *testing.T) {
	buf, err := ioutil.ReadFile("test/parser/BIEN-05.PL0")
	if err != nil {
		t.Errorf("Error open file test/parser/BIEN-05.PL0")
	}

	var wg sync.WaitGroup
	wg.Add(2)

	lexer := Lex("BIEN-05.PL0", string(buf), &wg)
	parser := Parse(lexer, &wg)

	wg.Wait()

	assert.Empty(t, parser.err)
}

func TestParseBien06(t *testing.T) {
	buf, err := ioutil.ReadFile("test/parser/BIEN-06.PL0")
	if err != nil {
		t.Errorf("Error open file test/parser/BIEN-06.PL0")
	}

	var wg sync.WaitGroup
	wg.Add(2)

	lexer := Lex("BIEN-06.PL0", string(buf), &wg)
	parser := Parse(lexer, &wg)

	wg.Wait()

	assert.Empty(t, parser.err)
}

func TestParseBien07(t *testing.T) {
	buf, err := ioutil.ReadFile("test/parser/BIEN-07.PL0")
	if err != nil {
		t.Errorf("Error open file test/parser/BIEN-07.PL0")
	}

	var wg sync.WaitGroup
	wg.Add(2)

	lexer := Lex("BIEN-07.PL0", string(buf), &wg)
	parser := Parse(lexer, &wg)

	wg.Wait()

	assert.Empty(t, parser.err)
}

func TestParseBien08(t *testing.T) {
	buf, err := ioutil.ReadFile("test/parser/BIEN-08.PL0")
	if err != nil {
		t.Errorf("Error open file test/parser/BIEN-08.PL0")
	}

	var wg sync.WaitGroup
	wg.Add(2)

	lexer := Lex("BIEN-08.PL0", string(buf), &wg)
	parser := Parse(lexer, &wg)

	wg.Wait()

	assert.Empty(t, parser.err)
}

func TestParseBien09(t *testing.T) {
	buf, err := ioutil.ReadFile("test/parser/BIEN-09.PL0")
	if err != nil {
		t.Errorf("Error open file test/parser/BIEN-09.PL0")
	}

	var wg sync.WaitGroup
	wg.Add(2)

	lexer := Lex("BIEN-09.PL0", string(buf), &wg)
	parser := Parse(lexer, &wg)

	wg.Wait()

	assert.Empty(t, parser.err)
}
