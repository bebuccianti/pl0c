package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"sync"
	"time"
)

func main() {
	start := time.Now()

	var debug = flag.Bool("d", false, "print debug output")
	flag.Parse()

	filename := flag.Arg(0)

	buf, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println("usage: pl0c <file>")
		os.Exit(1)
	}

	str := string(buf)

	var wg sync.WaitGroup
	wg.Add(2)

	lexer := Lex(filename, str, &wg)

	if *debug {
		for i := range lexer.items {
			fmt.Println(i)
		}
	}

	_ = Parse(lexer, &wg)

	wg.Wait()
	end := time.Now()
	elapsed := end.Sub(start)

	fmt.Println("Time elapsed: ", elapsed.Seconds(), "seconds")
}
