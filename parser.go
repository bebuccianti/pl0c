package main

import (
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
	"sync"
)

type parser struct {
	cg    Codegen
	s     Semantic
	lexer Lexer
	i     Item
	err   error
	line  int
	col   int
}

var i Item

func Parse(l *Lexer, wg *sync.WaitGroup) *parser {
	p := &parser{
		cg:    *CodeGenerator(l.name),
		s:     *NewSemantic(),
		lexer: *l,
		i:     <-l.items,
		err:   nil,
		line:  0,
		col:   0,
	}

	go p.run(wg)
	return p
}

func (p *parser) run(wg *sync.WaitGroup) {
	defer wg.Done()

	p.cg.loadByte(0xBF) // initialize EDI register
	p.cg.loadInt(0x00)

	p.program()

	p.cg.adjustment(p.s.nextVar)

	p.cg.fill()

	fmt.Println("Compiled successfully")
}

func (p *parser) program() {
	p.block(0)

	if p.i.typ != ItemDot {
		p.line, p.col = p.i.line, p.i.col
		p.err = errors.New("expected '.'")
		p.abort()
	}
}

func (p *parser) block(base int) {
	despl := 0

	p.cg.loadByte(0xE9)
	p.cg.loadInt(0)
	old := p.cg.getTopmem()

	if p.i.typ == ItemConst {
		for {
			p.nextItem()
			if p.i.typ != ItemIdentifier {
				p.line, p.col = p.i.line, p.i.col
				p.err = errors.New("expected identifier")
				p.abort()
			}
			id := p.i

			p.nextItem()
			if p.i.typ != ItemEqual && p.i.typ != ItemAssign {
				p.line, p.col = p.i.line, p.i.col
				p.err = errors.New("expected identifier")
				p.abort()
			}

			p.nextItem()
			if p.i.typ != ItemNumber {
				p.line, p.col = p.i.line, p.i.col
				p.err = errors.New("expected number")
				p.abort()
			}
			value, err := strconv.Atoi(p.i.val)
			if err != nil {
				p.line, p.col = p.i.line, p.i.col
				p.err = err
				p.abort()
			}

			if p.s.add(base, despl, value, id.val, ItemConst) {
				p.line, p.col = id.line, id.col
				p.err = errors.New("identifier duplicate")
				p.abort()
			}
			despl++

			p.nextItem()
			if p.i.typ != ItemComma {
				break
			}
		}
		if p.i.typ != ItemSemicolon {
			p.line, p.col = p.i.line, p.i.col
			p.err = errors.New("expected ';'")
			p.abort()
		}
		p.nextItem()
	}

	if p.i.typ == ItemVar {
		for {
			p.nextItem()
			if p.i.typ != ItemIdentifier {
				p.line, p.col = p.i.line, p.i.col
				p.err = errors.New("expected identifier")
				p.abort()
			}
			id := p.i

			p.nextItem()

			if p.s.add(base, despl, 0, id.val, ItemVar) {
				p.line, p.col = id.line, id.col
				p.err = errors.New("identifier duplicate")
				p.abort()
			}
			despl++

			if p.i.typ != ItemComma {
				break
			}
		}
		if p.i.typ != ItemSemicolon {
			p.line, p.col = p.i.line, p.i.col
			p.err = errors.New("expected ';'")
			p.abort()
		}
		p.nextItem()
	}

	for p.i.typ == ItemProcedure {
		p.nextItem()
		if p.i.typ != ItemIdentifier {
			p.line, p.col = p.i.line, p.i.col
			p.err = errors.New("expected identifier")
			p.abort()
		}
		id := p.i

		p.nextItem()

		if p.s.add(base, despl, p.cg.getTopmem(), id.val, ItemProcedure) {
			p.line, p.col = id.line, id.col
			p.err = errors.New("identifier duplicate")
			p.abort()
		}
		despl++

		if p.i.typ != ItemSemicolon {
			p.line, p.col = p.i.line, p.i.col
			p.err = errors.New("expected ';'")
			p.abort()
		}
		p.nextItem()

		p.block(base + despl)

		p.cg.loadByte(0xC3) // RET

		if p.i.typ != ItemSemicolon {
			p.line, p.col = p.i.line, p.i.col
			p.err = errors.New("expected ';'")
			p.abort()
		}
		p.nextItem()
	}

	p.cg.fixProcedure(old)

	p.proposition(base, despl)
}

func (p *parser) proposition(base, despl int) {
	switch p.i.typ {
	case ItemIdentifier:
		val := strings.ToLower(p.i.val)
		p.nextItem()
		if p.i.typ != ItemAssign {
			p.line, p.col = p.i.line, p.i.col
			p.err = errors.New("expected an assignment")
			p.abort()
		}

		p.nextItem()
		p.expression(base, despl)

		value, _, _ := p.s.getValue(base, despl, val)

		p.cg.loadVar(value)

		break

	case ItemCall:
		p.nextItem()
		if p.i.typ != ItemIdentifier {
			p.line, p.col = p.i.line, p.i.col
			p.err = errors.New("expected identifier")
			p.abort()
		}

		if !p.s.lookup(base, despl, p.i.val) {
			p.line, p.col = p.i.line, p.i.col
			p.err = errors.New("invoking invalid identifier")
			p.abort()
		}

		dir, _, _ := p.s.getValue(base, despl, p.i.val)

		p.cg.loadByte(0xE8)
		jump := dir - (p.cg.getTopmem() + 4)
		p.cg.loadInt(jump)

		p.nextItem()
		break

	case ItemBegin:
		p.nextItem()
		p.proposition(base, despl)

		for p.i.typ == ItemSemicolon {
			p.nextItem()
			p.proposition(base, despl)
		}

		if p.i.typ != ItemEnd {
			p.line, p.col = p.i.line, p.i.col
			p.err = errors.New("expected 'end' or ';'")
			p.abort()
		}

		p.nextItem()
		break

	case ItemIf:
		p.nextItem()
		p.condition(base, despl)

		before := p.cg.getTopmem()

		if p.i.typ != ItemThen {
			p.line, p.col = p.i.line, p.i.col
			p.err = errors.New("expected 'then'")
			p.abort()
		}

		p.nextItem()
		p.proposition(base, despl)

		space := p.cg.getTopmem() - before
		p.cg.loadIntPosition(space, before-4)
		break

	case ItemWhile:
		p.nextItem()

		before := p.cg.getTopmem()

		p.condition(base, despl)

		after := p.cg.getTopmem()

		if p.i.typ != ItemDo {
			p.line, p.col = p.i.line, p.i.col
			p.err = errors.New("expected 'do'")
			p.abort()
		}

		p.nextItem()
		p.proposition(base, despl)

		p.cg.loadByte(0xE9) // JMP ...
		p.cg.loadInt(before - (p.cg.getTopmem() + 4))

		space := p.cg.getTopmem() - after
		p.cg.loadIntPosition(space, after-4)

		break

	case ItemReadln:
		p.nextItem()
		if p.i.typ != ItemOpenParen {
			p.line, p.col = p.i.line, p.i.col
			p.err = errors.New("expected '('")
			p.abort()
		}

		for {
			p.nextItem()
			if p.i.typ != ItemIdentifier {
				p.line, p.col = p.i.line, p.i.col
				p.err = errors.New("expected identifier")
				p.abort()
			}

			offset, _, _ := p.s.getValue(base, despl, p.i.val)
			p.cg.handleReadln(offset)

			p.nextItem()
			if p.i.typ != ItemComma {
				break
			}
		}

		if p.i.typ != ItemCloseParen {
			p.line, p.col = p.i.line, p.i.col
			p.err = errors.New("expected ')'")
			p.abort()
		}

		p.nextItem()
		break

	case ItemWrite:
		p.nextItem()
		p.commonWrite(base, despl, ItemWrite)
		break

	case ItemWriteln:
		p.nextItem()
		if p.i.typ == ItemOpenParen {
			p.commonWrite(base, despl, ItemWriteln)
		} else {
			p.cg.printLine()
		}
		break

	case ItemInc:
		p.nextItem()
		if p.i.typ != ItemOpenParen {
			p.line, p.col = p.i.line, p.i.col
			p.err = errors.New("expected '('")
			p.abort()
		}

		p.nextItem()
		if p.i.typ != ItemIdentifier {
			p.line, p.col = p.i.line, p.i.col
			p.err = errors.New("expected identifier")
			p.abort()
		}

		offset, _, ok := p.s.getValue(base, despl, p.i.val)
		if !ok {
			p.line, p.col = p.i.line, p.i.col
			p.err = errors.New("wrong identifier")
			p.abort()
		}

		p.cg.handleInc(offset)

		p.nextItem()
		if p.i.typ != ItemCloseParen {
			p.line, p.col = p.i.line, p.i.col
			p.err = errors.New("expected ')'")
			p.abort()
		}
		p.nextItem()
		break
	}
}

func (p *parser) commonWrite(base, despl int, typ ItemType) {
	if p.i.typ != ItemOpenParen {
		p.line, p.col = p.i.line, p.i.col
		p.err = errors.New("expected '('")
		p.abort()
	}

	for {
		p.nextItem()
		if p.i.typ == ItemString {
			p.cg.handleString(p.i.val)
			p.nextItem()

		} else {
			p.expression(base, despl)
			p.cg.printNumber()
		}

		if p.i.typ != ItemComma {
			break
		}
	}

	if p.i.typ != ItemCloseParen {
		p.line, p.col = p.i.line, p.i.col
		p.err = errors.New("expected ')'")
		p.abort()
	}

	if typ == ItemWriteln {
		p.cg.printLine()
	}

	p.nextItem()
}

func (p *parser) condition(base, despl int) {
	if p.i.typ == ItemOdd {
		p.nextItem()
		p.expression(base, despl)
		p.cg.handleOdd()

	} else {
		p.expression(base, despl)

		op := p.i.typ

		switch p.i.typ {
		case ItemLessThan:
			p.nextItem()
			break
		case ItemGreaterThan:
			p.nextItem()
			break
		case ItemLessThanOrEqual:
			p.nextItem()
			break
		case ItemGreaterThanOrEqual:
			p.nextItem()
			break
		case ItemDifferent:
			p.nextItem()
			break
		case ItemEqual:
			p.nextItem()
			break
		default:
			p.line, p.col = p.i.line, p.i.col
			p.err = errors.New("expected logic operator")
			p.abort()

		}

		p.expression(base, despl)

		p.cg.handleCondition(op)
	}
}

func (p *parser) expression(base, despl int) {
	typ := ItemPlus
	if p.i.typ == ItemMinus || p.i.typ == ItemPlus {
		typ = p.i.typ
		p.nextItem()
	}
	p.term(base, despl)

	if typ == ItemMinus {
		p.cg.termNegate()
	}

	for p.i.typ == ItemMinus || p.i.typ == ItemPlus {
		op := p.i.typ
		p.nextItem()
		p.term(base, despl)
		switch op {
		case ItemPlus:
			p.cg.termPlus()
			break
		case ItemMinus:
			p.cg.termMinus()
			break
		}
	}
}

func (p *parser) term(base, despl int) {
	p.factor(base, despl)

	for p.i.typ == ItemTimes || p.i.typ == ItemDivide {
		op := p.i.typ

		p.nextItem()
		p.factor(base, despl)

		switch op {
		case ItemTimes:
			p.cg.termTimes()
			break
		case ItemDivide:
			p.cg.termDivide()
			break
		}
	}
}

func (p *parser) factor(base, despl int) {
	switch p.i.typ {
	case ItemIdentifier:
		upper := 0
		n, typ, ok := p.s.getValue(base, despl, p.i.val)

		if !ok {
			n2, _, ok2 := p.s.getValue(base, despl, strings.ToLower(p.i.val))

			if !ok2 {
				n3, _, ok3 := p.s.getValue(base, despl, strings.ToUpper(p.i.val))

				if !ok3 {
					p.line, p.col = p.i.line, p.i.col
					p.err = errors.New("identifier invalid")
					p.abort()
				}
				upper = n3
			}
			n = n2
		}

		if upper != 0 {
			n = upper
		}

		if typ == ItemVar {
			p.cg.handleVar(n)

		} else {
			p.cg.pushNumber(n)
		}

		p.nextItem()
		break

	case ItemNumber:
		value, err := strconv.Atoi(p.i.val)
		if err != nil {
			p.err = err
			p.abort()
		}
		p.cg.pushNumber(value)
		p.nextItem()
		break

	case ItemOpenParen:
		p.nextItem()
		p.expression(base, despl)

		if p.i.typ != ItemCloseParen {
			p.line, p.col = p.i.line, p.i.col
			p.err = errors.New("expected ')'")
			p.abort()
		}
		p.nextItem()
		break

	default:
		p.line, p.col = p.i.line, p.i.col
		p.err = errors.New("expected id, number or expression")
		p.abort()
	}
}

func (p *parser) nextItem() {
	p.i = <-p.lexer.items
}

func (p *parser) abort() {
	fmt.Print(p.lexer.name, ":", p.line, ":", p.col, ": ")
	fmt.Println(p.err)
	os.Exit(1)
}
