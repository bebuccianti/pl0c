package main

type bean struct {
	name string
	typ  ItemType
	val  int
}

type Semantic struct {
	table   []bean
	nextVar int
}

func NewSemantic() *Semantic {
	s := &Semantic{
		table:   make([]bean, 32767),
		nextVar: 0,
	}

	return s
}

func (s *Semantic) add(b, d, val int, n string, t ItemType) bool {
	for i := b; i <= b+d-1; i++ {
		if s.table[i].name == n {
			return true
		}
	}
	if t == ItemVar {
		s.table[b+d] = bean{n, t, s.nextVar}
		s.nextVar += 4

	} else {
		s.table[b+d] = bean{n, t, val}
	}

	return false
}

func (s *Semantic) lookup(base, despl int, name string) bool {
	for i := base + despl - 1; i >= 0; i-- {
		if s.table[i].name == name {
			return true
		}
	}
	return false
}

func (s *Semantic) getValue(base, despl int, name string) (int, ItemType, bool) {
	for i := base + despl - 1; i >= 0; i-- {
		if s.table[i].name == name {
			return s.table[i].val, s.table[i].typ, true
		}
	}

	return 0, ItemEOF, false
}
