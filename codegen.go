package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

type Codegen struct {
	memory []byte
	topmem int
	name   string
}

func CodeGenerator(filename string) *Codegen {
	cg := &Codegen{
		memory: make([]byte, 32768),
		topmem: 0,
		name:   filename[:strings.LastIndex(filename, ".")],
	}

	cg.load()

	return cg
}

func (cg *Codegen) getTopmem() int {
	return cg.topmem
}

func (cg *Codegen) loadByte(i byte) {
	cg.memory[cg.topmem] = i
	cg.topmem++
}

func (cg *Codegen) loadBytePosition(i byte, p int) {
	cg.memory[p] = i
}

func (cg *Codegen) loadInt(i int) {
	n := int64(i)

	if n < 0 {
		n = int64(i + 0x100000000)
	}

	cg.memory[cg.topmem] = byte(n % 256)
	cg.topmem++
	cg.memory[cg.topmem] = byte((n >> 8) % 256)
	cg.topmem++
	cg.memory[cg.topmem] = byte((n >> 16) % 256)
	cg.topmem++
	cg.memory[cg.topmem] = byte((n >> 24) % 256)
	cg.topmem++
}

func (cg *Codegen) loadIntPosition(i, p int) {
	n := int64(i)

	if n < 0 {
		n = int64(i + 0x100000000)
	}

	cg.memory[p] = byte(n % 256)
	cg.memory[p+1] = byte((n >> 8) % 256)
	cg.memory[p+2] = byte((n >> 16) % 256)
	cg.memory[p+3] = byte((n >> 24) % 256)
}

func (cg *Codegen) popEAX() {
	if cg.memory[cg.topmem-1] == 0x50 {
		cg.topmem -= 1

	} else {
		cg.loadByte(0x58) // POP EAX
	}
}

func (cg *Codegen) termPlus() {
	cg.popEAX()
	cg.loadByte(0x5B) // POP EBX
	cg.loadByte(0x01) // ADD EAX, EBX
	cg.loadByte(0xD8) // ''
	cg.loadByte(0x50) // PUSH EAX
}

func (cg *Codegen) termMinus() {
	cg.popEAX()
	cg.loadByte(0x5B) // POP EBX
	cg.loadByte(0x93) // XCHG EAX, EBX
	cg.loadByte(0x29) // SUB EAX, EBX
	cg.loadByte(0xD8) // ''
	cg.loadByte(0x50) // PUSH EAX
}

func (cg *Codegen) termTimes() {
	cg.popEAX()
	cg.loadByte(0x5B) // POP EBX
	cg.loadByte(0xF7) // IMUL EBX
	cg.loadByte(0xEB) // ''
	cg.loadByte(0x50) // PUSH EAX
}

func (cg *Codegen) termDivide() {
	cg.popEAX()
	cg.loadByte(0x5B) // POP EBX
	cg.loadByte(0x93) // XCHG EAX, EBX
	cg.loadByte(0x99) // CDQ
	cg.loadByte(0xF7) // IDIV EBX
	cg.loadByte(0xFB) // ''
	cg.loadByte(0x50) // PUSH EAX
}

func (cg *Codegen) termNegate() {
	cg.popEAX()
	cg.loadByte(0xF7) // NEG EAX
	cg.loadByte(0xD8) // ''
	cg.loadByte(0x50) // PUSH EAX
}

func (cg *Codegen) handleVar(offset int) {
	cg.loadByte(0x8B) // MOV EAX, [EDI + ...]
	cg.loadByte(0x87) // ''
	cg.loadInt(offset)
	cg.loadByte(0x50) // PUSH EAX
}

func (cg *Codegen) loadVar(offset int) {
	cg.popEAX()
	cg.loadByte(0x89)  // MOV [EDI+...], EAX
	cg.loadByte(0x87)  // ''
	cg.loadInt(offset) // offset
}

func (cg *Codegen) handleInc(offset int) {
	cg.handleVar(offset)

	cg.loadByte(0xB8) // MOV EAX, ...
	cg.loadInt(0x1)   //        , 01
	cg.loadByte(0x5B) // POP EBX
	cg.loadByte(0x01) // ADD EAX, EBX
	cg.loadByte(0xD8) // ''
	cg.loadByte(0x50) // PUSH EAX

	cg.loadVar(offset)
}

func (cg *Codegen) handleReadln(offset int) {
	cg.loadByte(0xE8)                   // CALL ...
	cg.loadInt(0x310 - (cg.topmem + 4)) // address to get number
	cg.loadByte(0x89)                   // MOV [EDI+...], EAX
	cg.loadByte(0x87)                   // ''
	cg.loadInt(offset)                  // dir
}

func (cg *Codegen) pushNumber(i int) {
	cg.loadByte(0xB8) // MOV EAX, ...
	cg.loadInt(i)
	cg.loadByte(0x50) // PUSH EAX
}

func (cg *Codegen) pushVar(i int) {
	cg.loadByte(0xB8) // MOV EAX
}

func (cg *Codegen) printNumber() {
	cg.popEAX()
	cg.loadByte(0xE8)                   // CALL ...
	cg.loadInt(0x190 - (cg.topmem + 4)) // address to print number
}

func (cg *Codegen) printLine() {
	cg.loadByte(0xE8)                   // CALL ...
	cg.loadInt(0x180 - (cg.topmem + 4)) // address to print line
}

func (cg *Codegen) handleString(s string) {
	address := cg.fetchInt(193)
	offset := cg.fetchInt(197)

	future := address - offset + cg.topmem + 20

	cg.loadByte(0xB9)                   // MOV ECX, ...
	cg.loadInt(future)                  // calculate with address field
	cg.loadByte(0xBA)                   // MOV EDX, ...
	cg.loadInt(len(s))                  // len of string
	cg.loadByte(0xE8)                   // CALL dir
	cg.loadInt(0x170 - (cg.topmem + 4)) // ''
	cg.loadByte(0xE9)                   // JMP dir
	cg.loadInt(len(s))

	for r := 0; r < len(s); r++ {
		cg.loadByte(s[r])
	}
}

func (cg *Codegen) handleOdd() {
	cg.popEAX()
	cg.loadByte(0xA8) // TEST AL, ...
	cg.loadByte(0x1)  // ''       1
	cg.loadByte(0x7B) // JPO ...
	cg.loadByte(0x5)  // ''  5
	cg.loadByte(0xE9) // JMP ...
	cg.loadInt(0)     // ''  dir
}

func (cg *Codegen) handleCondition(op ItemType) {
	cg.popEAX()
	cg.loadByte(0x5B) // POP EBX
	cg.loadByte(0x39) // CMP EBX, EAX
	cg.loadByte(0xC3) // ''

	switch op {
	case ItemEqual:
		cg.loadByte(0x74)
		break

	case ItemDifferent:
		cg.loadByte(0x75)
		break

	case ItemLessThan:
		cg.loadByte(0x7C)
		break

	case ItemLessThanOrEqual:
		cg.loadByte(0x7E)
		break

	case ItemGreaterThan:
		cg.loadByte(0x7F)
		break

	case ItemGreaterThanOrEqual:
		cg.loadByte(0x7D)
		break
	}

	cg.loadByte(0x05) //
	cg.loadByte(0xE9) // JMP ...
	cg.loadInt(0)     // ''  dir
}

func (cg *Codegen) fixProcedure(old int) {
	jmp := cg.topmem - old

	if jmp > 0 {
		cg.loadIntPosition(jmp, old-4)
	} else {
		cg.topmem -= 5
	}
}

func (cg *Codegen) fetchInt(addr int) int {
	address := 0
	var cor byte

	if cg.memory[addr+3] < 0 {
		cor = cg.memory[addr+3] + 0xFF + 1
	} else {
		cor = cg.memory[addr+3]
	}
	address += int(cor) * 256 * 256 * 256

	if cg.memory[addr+2] < 0 {
		cor = cg.memory[addr+2] + 0xFF + 1
	} else {
		cor = cg.memory[addr+2]
	}
	address += int(cor) * 256 * 256

	if cg.memory[addr+1] < 0 {
		cor = cg.memory[addr+1] + 0xFF + 1
	} else {
		cor = cg.memory[addr+1]
	}
	address += int(cor) * 256

	if cg.memory[addr] < 0 {
		cor = cg.memory[addr] + 0xFF + 1
	} else {
		cor = cg.memory[addr]
	}
	address += int(cor)

	return address
}

func (cg *Codegen) adjustment(nextVar int) {
	cg.loadIntPosition(cg.fetchInt(64)+cg.topmem+5, 0x481)
	cg.loadByte(0xE9)
	cg.loadInt(0x300 - (cg.topmem + 4))
	for i := 0; i < nextVar; i++ {
		cg.loadByte(0)
	}
	cg.loadIntPosition(cg.topmem, 68)
	cg.loadIntPosition(cg.topmem, 72)
	cg.loadIntPosition(cg.topmem-0xe0, 201)
}

func (cg *Codegen) fill() {
	err := ioutil.WriteFile(cg.name, cg.memory[:cg.topmem], 0744)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func (cg *Codegen) load() {
	cg.memory[cg.topmem] = 0x7f
	cg.topmem++
	cg.memory[cg.topmem] = 0x45
	cg.topmem++
	cg.memory[cg.topmem] = 0x4c
	cg.topmem++
	cg.memory[cg.topmem] = 0x46
	cg.topmem++
	cg.memory[cg.topmem] = 0x01
	cg.topmem++
	cg.memory[cg.topmem] = 0x01
	cg.topmem++
	cg.memory[cg.topmem] = 0x01
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00

	cg.topmem++
	cg.memory[cg.topmem] = 0x02
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x03
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x01
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0x84
	cg.topmem++
	cg.memory[cg.topmem] = 0x04
	cg.topmem++
	cg.memory[cg.topmem] = 0x08
	cg.topmem++
	cg.memory[cg.topmem] = 0x34
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00

	cg.topmem++
	cg.memory[cg.topmem] = 0x65
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x34
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x20
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x01
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x28
	cg.topmem++
	cg.memory[cg.topmem] = 0x00

	cg.topmem++
	cg.memory[cg.topmem] = 0x03
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x01
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x01
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0x04
	cg.topmem++
	cg.memory[cg.topmem] = 0x08

	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0x04
	cg.topmem++
	cg.memory[cg.topmem] = 0x08
	cg.topmem++
	cg.memory[cg.topmem] = 0x0a
	cg.topmem++
	cg.memory[cg.topmem] = 0x05
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x0a
	cg.topmem++
	cg.memory[cg.topmem] = 0x05
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x07
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00

	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x10
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x2e
	cg.topmem++
	cg.memory[cg.topmem] = 0x73
	cg.topmem++
	cg.memory[cg.topmem] = 0x68
	cg.topmem++
	cg.memory[cg.topmem] = 0x73
	cg.topmem++
	cg.memory[cg.topmem] = 0x74
	cg.topmem++
	cg.memory[cg.topmem] = 0x72
	cg.topmem++
	cg.memory[cg.topmem] = 0x74
	cg.topmem++
	cg.memory[cg.topmem] = 0x61
	cg.topmem++
	cg.memory[cg.topmem] = 0x62
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x2e

	cg.topmem++
	cg.memory[cg.topmem] = 0x74
	cg.topmem++
	cg.memory[cg.topmem] = 0x65
	cg.topmem++
	cg.memory[cg.topmem] = 0x78
	cg.topmem++
	cg.memory[cg.topmem] = 0x74
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00

	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00

	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x01
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00

	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x03
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x54
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00

	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x11
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x01
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00

	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x0b
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x01
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x06
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00

	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xe0
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0x04
	cg.topmem++
	cg.memory[cg.topmem] = 0x08
	cg.topmem++
	cg.memory[cg.topmem] = 0xe0
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x2a
	cg.topmem++
	cg.memory[cg.topmem] = 0x04
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00

	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x01
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00

	cg.topmem++
	cg.memory[cg.topmem] = 0x52
	cg.topmem++
	cg.memory[cg.topmem] = 0x51
	cg.topmem++
	cg.memory[cg.topmem] = 0x53
	cg.topmem++
	cg.memory[cg.topmem] = 0x50
	cg.topmem++
	cg.memory[cg.topmem] = 0xb8
	cg.topmem++
	cg.memory[cg.topmem] = 0x04
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xbb
	cg.topmem++
	cg.memory[cg.topmem] = 0x01
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x89
	cg.topmem++
	cg.memory[cg.topmem] = 0xe1

	cg.topmem++
	cg.memory[cg.topmem] = 0xba
	cg.topmem++
	cg.memory[cg.topmem] = 0x01
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xcd
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0x58
	cg.topmem++
	cg.memory[cg.topmem] = 0x5b
	cg.topmem++
	cg.memory[cg.topmem] = 0x59
	cg.topmem++
	cg.memory[cg.topmem] = 0x5a
	cg.topmem++
	cg.memory[cg.topmem] = 0xc3
	cg.topmem++
	cg.memory[cg.topmem] = 0x55
	cg.topmem++
	cg.memory[cg.topmem] = 0x89
	cg.topmem++
	cg.memory[cg.topmem] = 0xe5
	cg.topmem++
	cg.memory[cg.topmem] = 0x81

	cg.topmem++
	cg.memory[cg.topmem] = 0xec
	cg.topmem++
	cg.memory[cg.topmem] = 0x24
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x52
	cg.topmem++
	cg.memory[cg.topmem] = 0x51
	cg.topmem++
	cg.memory[cg.topmem] = 0x53
	cg.topmem++
	cg.memory[cg.topmem] = 0xb8
	cg.topmem++
	cg.memory[cg.topmem] = 0x36
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xbb
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00

	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xb9
	cg.topmem++
	cg.memory[cg.topmem] = 0x01
	cg.topmem++
	cg.memory[cg.topmem] = 0x54
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x8d
	cg.topmem++
	cg.memory[cg.topmem] = 0x55
	cg.topmem++
	cg.memory[cg.topmem] = 0xdc
	cg.topmem++
	cg.memory[cg.topmem] = 0xcd
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0x81
	cg.topmem++
	cg.memory[cg.topmem] = 0x65
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0xf5

	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xb8
	cg.topmem++
	cg.memory[cg.topmem] = 0x36
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xbb
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xb9
	cg.topmem++
	cg.memory[cg.topmem] = 0x02
	cg.topmem++
	cg.memory[cg.topmem] = 0x54

	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x8d
	cg.topmem++
	cg.memory[cg.topmem] = 0x55
	cg.topmem++
	cg.memory[cg.topmem] = 0xdc
	cg.topmem++
	cg.memory[cg.topmem] = 0xcd
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0x31
	cg.topmem++
	cg.memory[cg.topmem] = 0xc0
	cg.topmem++
	cg.memory[cg.topmem] = 0x50
	cg.topmem++
	cg.memory[cg.topmem] = 0xb8
	cg.topmem++
	cg.memory[cg.topmem] = 0x03
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xbb

	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x89
	cg.topmem++
	cg.memory[cg.topmem] = 0xe1
	cg.topmem++
	cg.memory[cg.topmem] = 0xba
	cg.topmem++
	cg.memory[cg.topmem] = 0x01
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xcd
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0x81
	cg.topmem++
	cg.memory[cg.topmem] = 0x4d
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8

	cg.topmem++
	cg.memory[cg.topmem] = 0x0a
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xb8
	cg.topmem++
	cg.memory[cg.topmem] = 0x36
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xbb
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xb9
	cg.topmem++
	cg.memory[cg.topmem] = 0x02

	cg.topmem++
	cg.memory[cg.topmem] = 0x54
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x8d
	cg.topmem++
	cg.memory[cg.topmem] = 0x55
	cg.topmem++
	cg.memory[cg.topmem] = 0xdc
	cg.topmem++
	cg.memory[cg.topmem] = 0xcd
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0x58
	cg.topmem++
	cg.memory[cg.topmem] = 0x5b
	cg.topmem++
	cg.memory[cg.topmem] = 0x59
	cg.topmem++
	cg.memory[cg.topmem] = 0x5a
	cg.topmem++
	cg.memory[cg.topmem] = 0x89
	cg.topmem++
	cg.memory[cg.topmem] = 0xec
	cg.topmem++
	cg.memory[cg.topmem] = 0x5d
	cg.topmem++
	cg.memory[cg.topmem] = 0xc3

	cg.topmem++
	cg.memory[cg.topmem] = 0xb8
	cg.topmem++
	cg.memory[cg.topmem] = 0x04
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xbb
	cg.topmem++
	cg.memory[cg.topmem] = 0x01
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xcd
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0xc3
	cg.topmem++
	cg.memory[cg.topmem] = 0x90
	cg.topmem++
	cg.memory[cg.topmem] = 0x90
	cg.topmem++
	cg.memory[cg.topmem] = 0x90

	cg.topmem++
	cg.memory[cg.topmem] = 0xb0
	cg.topmem++
	cg.memory[cg.topmem] = 0x0a
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0x59
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xc3
	cg.topmem++
	cg.memory[cg.topmem] = 0x04
	cg.topmem++
	cg.memory[cg.topmem] = 0x30
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0x51
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xc3

	cg.topmem++
	cg.memory[cg.topmem] = 0x3d
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0x75
	cg.topmem++
	cg.memory[cg.topmem] = 0x4e
	cg.topmem++
	cg.memory[cg.topmem] = 0xb0
	cg.topmem++
	cg.memory[cg.topmem] = 0x2d
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0x42
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xb0
	cg.topmem++
	cg.memory[cg.topmem] = 0x02

	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0xe3
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xb0
	cg.topmem++
	cg.memory[cg.topmem] = 0x01
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0xdc
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xb0
	cg.topmem++
	cg.memory[cg.topmem] = 0x04
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0xd5

	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xb0
	cg.topmem++
	cg.memory[cg.topmem] = 0x07
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0xce
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xb0
	cg.topmem++
	cg.memory[cg.topmem] = 0x04
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0xc7
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff

	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xb0
	cg.topmem++
	cg.memory[cg.topmem] = 0x08
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0xc0
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xb0
	cg.topmem++
	cg.memory[cg.topmem] = 0x03
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0xb9
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xb0

	cg.topmem++
	cg.memory[cg.topmem] = 0x06
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0xb2
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xb0
	cg.topmem++
	cg.memory[cg.topmem] = 0x04
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0xab
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xb0
	cg.topmem++
	cg.memory[cg.topmem] = 0x08
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8

	cg.topmem++
	cg.memory[cg.topmem] = 0xa4
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xc3
	cg.topmem++
	cg.memory[cg.topmem] = 0x3d
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x7d
	cg.topmem++
	cg.memory[cg.topmem] = 0x0b
	cg.topmem++
	cg.memory[cg.topmem] = 0x50
	cg.topmem++
	cg.memory[cg.topmem] = 0xb0
	cg.topmem++
	cg.memory[cg.topmem] = 0x2d
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8

	cg.topmem++
	cg.memory[cg.topmem] = 0xec
	cg.topmem++
	cg.memory[cg.topmem] = 0xfe
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0x58
	cg.topmem++
	cg.memory[cg.topmem] = 0xf7
	cg.topmem++
	cg.memory[cg.topmem] = 0xd8
	cg.topmem++
	cg.memory[cg.topmem] = 0x3d
	cg.topmem++
	cg.memory[cg.topmem] = 0x0a
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x0f
	cg.topmem++
	cg.memory[cg.topmem] = 0x8c
	cg.topmem++
	cg.memory[cg.topmem] = 0xef
	cg.topmem++
	cg.memory[cg.topmem] = 0x00

	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x3d
	cg.topmem++
	cg.memory[cg.topmem] = 0x64
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x0f
	cg.topmem++
	cg.memory[cg.topmem] = 0x8c
	cg.topmem++
	cg.memory[cg.topmem] = 0xd1
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x3d
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0x03

	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x0f
	cg.topmem++
	cg.memory[cg.topmem] = 0x8c
	cg.topmem++
	cg.memory[cg.topmem] = 0xb3
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x3d
	cg.topmem++
	cg.memory[cg.topmem] = 0x10
	cg.topmem++
	cg.memory[cg.topmem] = 0x27
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x0f
	cg.topmem++
	cg.memory[cg.topmem] = 0x8c
	cg.topmem++
	cg.memory[cg.topmem] = 0x95

	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x3d
	cg.topmem++
	cg.memory[cg.topmem] = 0xa0
	cg.topmem++
	cg.memory[cg.topmem] = 0x86
	cg.topmem++
	cg.memory[cg.topmem] = 0x01
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x7c
	cg.topmem++
	cg.memory[cg.topmem] = 0x7b
	cg.topmem++
	cg.memory[cg.topmem] = 0x3d
	cg.topmem++
	cg.memory[cg.topmem] = 0x40
	cg.topmem++
	cg.memory[cg.topmem] = 0x42
	cg.topmem++
	cg.memory[cg.topmem] = 0x0f
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x7c

	cg.topmem++
	cg.memory[cg.topmem] = 0x61
	cg.topmem++
	cg.memory[cg.topmem] = 0x3d
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0x96
	cg.topmem++
	cg.memory[cg.topmem] = 0x98
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x7c
	cg.topmem++
	cg.memory[cg.topmem] = 0x47
	cg.topmem++
	cg.memory[cg.topmem] = 0x3d
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xe1
	cg.topmem++
	cg.memory[cg.topmem] = 0xf5
	cg.topmem++
	cg.memory[cg.topmem] = 0x05
	cg.topmem++
	cg.memory[cg.topmem] = 0x7c
	cg.topmem++
	cg.memory[cg.topmem] = 0x2d
	cg.topmem++
	cg.memory[cg.topmem] = 0x3d

	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xca
	cg.topmem++
	cg.memory[cg.topmem] = 0x9a
	cg.topmem++
	cg.memory[cg.topmem] = 0x3b
	cg.topmem++
	cg.memory[cg.topmem] = 0x7c
	cg.topmem++
	cg.memory[cg.topmem] = 0x13
	cg.topmem++
	cg.memory[cg.topmem] = 0xba
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xbb
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xca
	cg.topmem++
	cg.memory[cg.topmem] = 0x9a
	cg.topmem++
	cg.memory[cg.topmem] = 0x3b

	cg.topmem++
	cg.memory[cg.topmem] = 0xf7
	cg.topmem++
	cg.memory[cg.topmem] = 0xfb
	cg.topmem++
	cg.memory[cg.topmem] = 0x52
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0x30
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0x58
	cg.topmem++
	cg.memory[cg.topmem] = 0xba
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xbb
	cg.topmem++
	cg.memory[cg.topmem] = 0x00

	cg.topmem++
	cg.memory[cg.topmem] = 0xe1
	cg.topmem++
	cg.memory[cg.topmem] = 0xf5
	cg.topmem++
	cg.memory[cg.topmem] = 0x05
	cg.topmem++
	cg.memory[cg.topmem] = 0xf7
	cg.topmem++
	cg.memory[cg.topmem] = 0xfb
	cg.topmem++
	cg.memory[cg.topmem] = 0x52
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0x1d
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0x58
	cg.topmem++
	cg.memory[cg.topmem] = 0xba
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00

	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xbb
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0x96
	cg.topmem++
	cg.memory[cg.topmem] = 0x98
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xf7
	cg.topmem++
	cg.memory[cg.topmem] = 0xfb
	cg.topmem++
	cg.memory[cg.topmem] = 0x52
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0x0a
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0x58
	cg.topmem++
	cg.memory[cg.topmem] = 0xba

	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xbb
	cg.topmem++
	cg.memory[cg.topmem] = 0x40
	cg.topmem++
	cg.memory[cg.topmem] = 0x42
	cg.topmem++
	cg.memory[cg.topmem] = 0x0f
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xf7
	cg.topmem++
	cg.memory[cg.topmem] = 0xfb
	cg.topmem++
	cg.memory[cg.topmem] = 0x52
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0xf7
	cg.topmem++
	cg.memory[cg.topmem] = 0xfe
	cg.topmem++
	cg.memory[cg.topmem] = 0xff

	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0x58
	cg.topmem++
	cg.memory[cg.topmem] = 0xba
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xbb
	cg.topmem++
	cg.memory[cg.topmem] = 0xa0
	cg.topmem++
	cg.memory[cg.topmem] = 0x86
	cg.topmem++
	cg.memory[cg.topmem] = 0x01
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xf7
	cg.topmem++
	cg.memory[cg.topmem] = 0xfb
	cg.topmem++
	cg.memory[cg.topmem] = 0x52
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8

	cg.topmem++
	cg.memory[cg.topmem] = 0xe4
	cg.topmem++
	cg.memory[cg.topmem] = 0xfe
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0x58
	cg.topmem++
	cg.memory[cg.topmem] = 0xba
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xbb
	cg.topmem++
	cg.memory[cg.topmem] = 0x10
	cg.topmem++
	cg.memory[cg.topmem] = 0x27
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xf7

	cg.topmem++
	cg.memory[cg.topmem] = 0xfb
	cg.topmem++
	cg.memory[cg.topmem] = 0x52
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0xd1
	cg.topmem++
	cg.memory[cg.topmem] = 0xfe
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0x58
	cg.topmem++
	cg.memory[cg.topmem] = 0xba
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xbb
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0x03

	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xf7
	cg.topmem++
	cg.memory[cg.topmem] = 0xfb
	cg.topmem++
	cg.memory[cg.topmem] = 0x52
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0xbe
	cg.topmem++
	cg.memory[cg.topmem] = 0xfe
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0x58
	cg.topmem++
	cg.memory[cg.topmem] = 0xba
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00

	cg.topmem++
	cg.memory[cg.topmem] = 0xbb
	cg.topmem++
	cg.memory[cg.topmem] = 0x64
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xf7
	cg.topmem++
	cg.memory[cg.topmem] = 0xfb
	cg.topmem++
	cg.memory[cg.topmem] = 0x52
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0xab
	cg.topmem++
	cg.memory[cg.topmem] = 0xfe
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0x58
	cg.topmem++
	cg.memory[cg.topmem] = 0xba
	cg.topmem++
	cg.memory[cg.topmem] = 0x00

	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xbb
	cg.topmem++
	cg.memory[cg.topmem] = 0x0a
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xf7
	cg.topmem++
	cg.memory[cg.topmem] = 0xfb
	cg.topmem++
	cg.memory[cg.topmem] = 0x52
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0x98
	cg.topmem++
	cg.memory[cg.topmem] = 0xfe
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff

	cg.topmem++
	cg.memory[cg.topmem] = 0x58
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0x92
	cg.topmem++
	cg.memory[cg.topmem] = 0xfe
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xc3
	cg.topmem++
	cg.memory[cg.topmem] = 0x90
	cg.topmem++
	cg.memory[cg.topmem] = 0x90
	cg.topmem++
	cg.memory[cg.topmem] = 0x90
	cg.topmem++
	cg.memory[cg.topmem] = 0x90
	cg.topmem++
	cg.memory[cg.topmem] = 0x90
	cg.topmem++
	cg.memory[cg.topmem] = 0x90
	cg.topmem++
	cg.memory[cg.topmem] = 0x90
	cg.topmem++
	cg.memory[cg.topmem] = 0x90
	cg.topmem++
	cg.memory[cg.topmem] = 0x90

	cg.topmem++
	cg.memory[cg.topmem] = 0xb8
	cg.topmem++
	cg.memory[cg.topmem] = 0x01
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xbb
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xcd
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0x90
	cg.topmem++
	cg.memory[cg.topmem] = 0x90
	cg.topmem++
	cg.memory[cg.topmem] = 0x90
	cg.topmem++
	cg.memory[cg.topmem] = 0x90

	cg.topmem++
	cg.memory[cg.topmem] = 0xb9
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xb3
	cg.topmem++
	cg.memory[cg.topmem] = 0x03
	cg.topmem++
	cg.memory[cg.topmem] = 0x51
	cg.topmem++
	cg.memory[cg.topmem] = 0x53
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0xde
	cg.topmem++
	cg.memory[cg.topmem] = 0xfd
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0x5b
	cg.topmem++
	cg.memory[cg.topmem] = 0x59

	cg.topmem++
	cg.memory[cg.topmem] = 0x3c
	cg.topmem++
	cg.memory[cg.topmem] = 0x0a
	cg.topmem++
	cg.memory[cg.topmem] = 0x0f
	cg.topmem++
	cg.memory[cg.topmem] = 0x84
	cg.topmem++
	cg.memory[cg.topmem] = 0x34
	cg.topmem++
	cg.memory[cg.topmem] = 0x01
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x3c
	cg.topmem++
	cg.memory[cg.topmem] = 0x7f
	cg.topmem++
	cg.memory[cg.topmem] = 0x0f
	cg.topmem++
	cg.memory[cg.topmem] = 0x84
	cg.topmem++
	cg.memory[cg.topmem] = 0x94
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00

	cg.topmem++
	cg.memory[cg.topmem] = 0x3c
	cg.topmem++
	cg.memory[cg.topmem] = 0x2d
	cg.topmem++
	cg.memory[cg.topmem] = 0x0f
	cg.topmem++
	cg.memory[cg.topmem] = 0x84
	cg.topmem++
	cg.memory[cg.topmem] = 0x09
	cg.topmem++
	cg.memory[cg.topmem] = 0x01
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x3c
	cg.topmem++
	cg.memory[cg.topmem] = 0x30
	cg.topmem++
	cg.memory[cg.topmem] = 0x7c
	cg.topmem++
	cg.memory[cg.topmem] = 0xdb
	cg.topmem++
	cg.memory[cg.topmem] = 0x3c
	cg.topmem++
	cg.memory[cg.topmem] = 0x39
	cg.topmem++
	cg.memory[cg.topmem] = 0x7f
	cg.topmem++
	cg.memory[cg.topmem] = 0xd7

	cg.topmem++
	cg.memory[cg.topmem] = 0x2c
	cg.topmem++
	cg.memory[cg.topmem] = 0x30
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0xfb
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x74
	cg.topmem++
	cg.memory[cg.topmem] = 0xd0
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0xfb
	cg.topmem++
	cg.memory[cg.topmem] = 0x02
	cg.topmem++
	cg.memory[cg.topmem] = 0x75
	cg.topmem++
	cg.memory[cg.topmem] = 0x0c
	cg.topmem++
	cg.memory[cg.topmem] = 0x81
	cg.topmem++
	cg.memory[cg.topmem] = 0xf9
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00

	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x75
	cg.topmem++
	cg.memory[cg.topmem] = 0x04
	cg.topmem++
	cg.memory[cg.topmem] = 0x3c
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x74
	cg.topmem++
	cg.memory[cg.topmem] = 0xbf
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0xfb
	cg.topmem++
	cg.memory[cg.topmem] = 0x03
	cg.topmem++
	cg.memory[cg.topmem] = 0x75
	cg.topmem++
	cg.memory[cg.topmem] = 0x0a
	cg.topmem++
	cg.memory[cg.topmem] = 0x3c
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x75

	cg.topmem++
	cg.memory[cg.topmem] = 0x04
	cg.topmem++
	cg.memory[cg.topmem] = 0xb3
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xeb
	cg.topmem++
	cg.memory[cg.topmem] = 0x02
	cg.topmem++
	cg.memory[cg.topmem] = 0xb3
	cg.topmem++
	cg.memory[cg.topmem] = 0x01
	cg.topmem++
	cg.memory[cg.topmem] = 0x81
	cg.topmem++
	cg.memory[cg.topmem] = 0xf9
	cg.topmem++
	cg.memory[cg.topmem] = 0xcc
	cg.topmem++
	cg.memory[cg.topmem] = 0xcc
	cg.topmem++
	cg.memory[cg.topmem] = 0xcc
	cg.topmem++
	cg.memory[cg.topmem] = 0x0c
	cg.topmem++
	cg.memory[cg.topmem] = 0x7f
	cg.topmem++
	cg.memory[cg.topmem] = 0xa8
	cg.topmem++
	cg.memory[cg.topmem] = 0x81

	cg.topmem++
	cg.memory[cg.topmem] = 0xf9
	cg.topmem++
	cg.memory[cg.topmem] = 0x34
	cg.topmem++
	cg.memory[cg.topmem] = 0x33
	cg.topmem++
	cg.memory[cg.topmem] = 0x33
	cg.topmem++
	cg.memory[cg.topmem] = 0xf3
	cg.topmem++
	cg.memory[cg.topmem] = 0x7c
	cg.topmem++
	cg.memory[cg.topmem] = 0xa0
	cg.topmem++
	cg.memory[cg.topmem] = 0x88
	cg.topmem++
	cg.memory[cg.topmem] = 0xc7
	cg.topmem++
	cg.memory[cg.topmem] = 0xb8
	cg.topmem++
	cg.memory[cg.topmem] = 0x0a
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xf7
	cg.topmem++
	cg.memory[cg.topmem] = 0xe9

	cg.topmem++
	cg.memory[cg.topmem] = 0x3d
	cg.topmem++
	cg.memory[cg.topmem] = 0x08
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0x74
	cg.topmem++
	cg.memory[cg.topmem] = 0x11
	cg.topmem++
	cg.memory[cg.topmem] = 0x3d
	cg.topmem++
	cg.memory[cg.topmem] = 0xf8
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0x7f
	cg.topmem++
	cg.memory[cg.topmem] = 0x75
	cg.topmem++
	cg.memory[cg.topmem] = 0x13
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0xff

	cg.topmem++
	cg.memory[cg.topmem] = 0x07
	cg.topmem++
	cg.memory[cg.topmem] = 0x7e
	cg.topmem++
	cg.memory[cg.topmem] = 0x0e
	cg.topmem++
	cg.memory[cg.topmem] = 0xe9
	cg.topmem++
	cg.memory[cg.topmem] = 0x7f
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0x08
	cg.topmem++
	cg.memory[cg.topmem] = 0x0f
	cg.topmem++
	cg.memory[cg.topmem] = 0x8f
	cg.topmem++
	cg.memory[cg.topmem] = 0x76
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff

	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xb9
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x88
	cg.topmem++
	cg.memory[cg.topmem] = 0xf9
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0xfb
	cg.topmem++
	cg.memory[cg.topmem] = 0x02
	cg.topmem++
	cg.memory[cg.topmem] = 0x74
	cg.topmem++
	cg.memory[cg.topmem] = 0x04
	cg.topmem++
	cg.memory[cg.topmem] = 0x01
	cg.topmem++
	cg.memory[cg.topmem] = 0xc1
	cg.topmem++
	cg.memory[cg.topmem] = 0xeb

	cg.topmem++
	cg.memory[cg.topmem] = 0x03
	cg.topmem++
	cg.memory[cg.topmem] = 0x29
	cg.topmem++
	cg.memory[cg.topmem] = 0xc8
	cg.topmem++
	cg.memory[cg.topmem] = 0x91
	cg.topmem++
	cg.memory[cg.topmem] = 0x88
	cg.topmem++
	cg.memory[cg.topmem] = 0xf8
	cg.topmem++
	cg.memory[cg.topmem] = 0x51
	cg.topmem++
	cg.memory[cg.topmem] = 0x53
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0xcb
	cg.topmem++
	cg.memory[cg.topmem] = 0xfd
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0x5b
	cg.topmem++
	cg.memory[cg.topmem] = 0x59
	cg.topmem++
	cg.memory[cg.topmem] = 0xe9

	cg.topmem++
	cg.memory[cg.topmem] = 0x53
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0xfb
	cg.topmem++
	cg.memory[cg.topmem] = 0x03
	cg.topmem++
	cg.memory[cg.topmem] = 0x0f
	cg.topmem++
	cg.memory[cg.topmem] = 0x84
	cg.topmem++
	cg.memory[cg.topmem] = 0x4a
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0x51
	cg.topmem++
	cg.memory[cg.topmem] = 0x53
	cg.topmem++
	cg.memory[cg.topmem] = 0xb0

	cg.topmem++
	cg.memory[cg.topmem] = 0x08
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0x0a
	cg.topmem++
	cg.memory[cg.topmem] = 0xfd
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xb0
	cg.topmem++
	cg.memory[cg.topmem] = 0x20
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0x03
	cg.topmem++
	cg.memory[cg.topmem] = 0xfd
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xb0
	cg.topmem++
	cg.memory[cg.topmem] = 0x08
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8

	cg.topmem++
	cg.memory[cg.topmem] = 0xfc
	cg.topmem++
	cg.memory[cg.topmem] = 0xfc
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0x5b
	cg.topmem++
	cg.memory[cg.topmem] = 0x59
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0xfb
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x75
	cg.topmem++
	cg.memory[cg.topmem] = 0x07
	cg.topmem++
	cg.memory[cg.topmem] = 0xb3
	cg.topmem++
	cg.memory[cg.topmem] = 0x03
	cg.topmem++
	cg.memory[cg.topmem] = 0xe9
	cg.topmem++
	cg.memory[cg.topmem] = 0x25
	cg.topmem++
	cg.memory[cg.topmem] = 0xff

	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0xfb
	cg.topmem++
	cg.memory[cg.topmem] = 0x02
	cg.topmem++
	cg.memory[cg.topmem] = 0x75
	cg.topmem++
	cg.memory[cg.topmem] = 0x0f
	cg.topmem++
	cg.memory[cg.topmem] = 0x81
	cg.topmem++
	cg.memory[cg.topmem] = 0xf9
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x75
	cg.topmem++
	cg.memory[cg.topmem] = 0x07
	cg.topmem++
	cg.memory[cg.topmem] = 0xb3

	cg.topmem++
	cg.memory[cg.topmem] = 0x03
	cg.topmem++
	cg.memory[cg.topmem] = 0xe9
	cg.topmem++
	cg.memory[cg.topmem] = 0x11
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0x89
	cg.topmem++
	cg.memory[cg.topmem] = 0xc8
	cg.topmem++
	cg.memory[cg.topmem] = 0xb9
	cg.topmem++
	cg.memory[cg.topmem] = 0x0a
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0xba
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00

	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x3d
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x7d
	cg.topmem++
	cg.memory[cg.topmem] = 0x08
	cg.topmem++
	cg.memory[cg.topmem] = 0xf7
	cg.topmem++
	cg.memory[cg.topmem] = 0xd8
	cg.topmem++
	cg.memory[cg.topmem] = 0xf7
	cg.topmem++
	cg.memory[cg.topmem] = 0xf9
	cg.topmem++
	cg.memory[cg.topmem] = 0xf7
	cg.topmem++
	cg.memory[cg.topmem] = 0xd8
	cg.topmem++
	cg.memory[cg.topmem] = 0xeb

	cg.topmem++
	cg.memory[cg.topmem] = 0x02
	cg.topmem++
	cg.memory[cg.topmem] = 0xf7
	cg.topmem++
	cg.memory[cg.topmem] = 0xf9
	cg.topmem++
	cg.memory[cg.topmem] = 0x89
	cg.topmem++
	cg.memory[cg.topmem] = 0xc1
	cg.topmem++
	cg.memory[cg.topmem] = 0x81
	cg.topmem++
	cg.memory[cg.topmem] = 0xf9
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x0f
	cg.topmem++
	cg.memory[cg.topmem] = 0x85
	cg.topmem++
	cg.memory[cg.topmem] = 0xe6
	cg.topmem++
	cg.memory[cg.topmem] = 0xfe
	cg.topmem++
	cg.memory[cg.topmem] = 0xff

	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0xfb
	cg.topmem++
	cg.memory[cg.topmem] = 0x02
	cg.topmem++
	cg.memory[cg.topmem] = 0x0f
	cg.topmem++
	cg.memory[cg.topmem] = 0x84
	cg.topmem++
	cg.memory[cg.topmem] = 0xdd
	cg.topmem++
	cg.memory[cg.topmem] = 0xfe
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xb3
	cg.topmem++
	cg.memory[cg.topmem] = 0x03
	cg.topmem++
	cg.memory[cg.topmem] = 0xe9
	cg.topmem++
	cg.memory[cg.topmem] = 0xd6
	cg.topmem++
	cg.memory[cg.topmem] = 0xfe
	cg.topmem++
	cg.memory[cg.topmem] = 0xff

	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0xfb
	cg.topmem++
	cg.memory[cg.topmem] = 0x03
	cg.topmem++
	cg.memory[cg.topmem] = 0x0f
	cg.topmem++
	cg.memory[cg.topmem] = 0x85
	cg.topmem++
	cg.memory[cg.topmem] = 0xcd
	cg.topmem++
	cg.memory[cg.topmem] = 0xfe
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xb0
	cg.topmem++
	cg.memory[cg.topmem] = 0x2d
	cg.topmem++
	cg.memory[cg.topmem] = 0x51
	cg.topmem++
	cg.memory[cg.topmem] = 0x53
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0x8d

	cg.topmem++
	cg.memory[cg.topmem] = 0xfc
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0x5b
	cg.topmem++
	cg.memory[cg.topmem] = 0x59
	cg.topmem++
	cg.memory[cg.topmem] = 0xb3
	cg.topmem++
	cg.memory[cg.topmem] = 0x02
	cg.topmem++
	cg.memory[cg.topmem] = 0xe9
	cg.topmem++
	cg.memory[cg.topmem] = 0xbb
	cg.topmem++
	cg.memory[cg.topmem] = 0xfe
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0xfb
	cg.topmem++
	cg.memory[cg.topmem] = 0x03
	cg.topmem++
	cg.memory[cg.topmem] = 0x0f

	cg.topmem++
	cg.memory[cg.topmem] = 0x84
	cg.topmem++
	cg.memory[cg.topmem] = 0xb2
	cg.topmem++
	cg.memory[cg.topmem] = 0xfe
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0x80
	cg.topmem++
	cg.memory[cg.topmem] = 0xfb
	cg.topmem++
	cg.memory[cg.topmem] = 0x02
	cg.topmem++
	cg.memory[cg.topmem] = 0x75
	cg.topmem++
	cg.memory[cg.topmem] = 0x0c
	cg.topmem++
	cg.memory[cg.topmem] = 0x81
	cg.topmem++
	cg.memory[cg.topmem] = 0xf9
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00
	cg.topmem++
	cg.memory[cg.topmem] = 0x00

	cg.topmem++
	cg.memory[cg.topmem] = 0x0f
	cg.topmem++
	cg.memory[cg.topmem] = 0x84
	cg.topmem++
	cg.memory[cg.topmem] = 0xa1
	cg.topmem++
	cg.memory[cg.topmem] = 0xfe
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0x51
	cg.topmem++
	cg.memory[cg.topmem] = 0xe8
	cg.topmem++
	cg.memory[cg.topmem] = 0x04
	cg.topmem++
	cg.memory[cg.topmem] = 0xfd
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0xff
	cg.topmem++
	cg.memory[cg.topmem] = 0x59
	cg.topmem++
	cg.memory[cg.topmem] = 0x89
	cg.topmem++
	cg.memory[cg.topmem] = 0xc8
	cg.topmem++
	cg.memory[cg.topmem] = 0xc3
	cg.topmem++
}
