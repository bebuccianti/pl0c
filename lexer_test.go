package main

import (
	"io/ioutil"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLexConst(t *testing.T) {
	buf, err := ioutil.ReadFile("test/lexer/const.pl0")
	if err != nil {
		t.Errorf("Error open file test/lexer/const.pl0")
	}

	table := []struct {
		iType ItemType
		iVal  string
	}{
		{ItemConst, "const"},
		{ItemIdentifier, "gaga"},
		{ItemEqual, "="},
		{ItemNumber, "10"},
		{ItemSemicolon, ";"},
		{ItemEOF, ""},
	}

	var wg sync.WaitGroup
	wg.Add(1)

	i := 0
	lexer := Lex("const.pl0", string(buf), &wg)

	for item := range lexer.items {
		assert.Equal(t, table[i].iType, item.typ)
		assert.Equal(t, table[i].iVal, item.val)
		i++
	}
}

func TestLexConstAdds(t *testing.T) {
	buf, err := ioutil.ReadFile("test/lexer/constAdds.pl0")
	if err != nil {
		t.Errorf("Error open file test/lexer/constAdds.pl0")
	}

	table := []struct {
		iType ItemType
		iVal  string
		iLine int
		iCol  int
	}{
		{ItemConst, "const", 1, 0},
		{ItemIdentifier, "gaga", 1, 6},
		{ItemEqual, "=", 1, 11},
		{ItemNumber, "10", 1, 13},
		{ItemComma, ",", 1, 15},
		{ItemIdentifier, "sonono", 1, 17},
		{ItemEqual, "=", 1, 24},
		{ItemNumber, "25", 1, 26},
		{ItemSemicolon, ";", 1, 28},
		{ItemEOF, "", 1, 29},
	}

	var wg sync.WaitGroup
	wg.Add(1)

	i := 0
	lexer := Lex("constAdds.pl0", string(buf), &wg)
	for item := range lexer.items {
		assert.Equal(t, table[i].iType, item.typ)
		assert.Equal(t, table[i].iVal, item.val)
		assert.Equal(t, table[i].iLine, item.line, "line must be equal")
		assert.Equal(t, table[i].iCol, item.col, "col must be equal")
		i++
	}
}

func TestLexConstsUpperPlusNumbers(t *testing.T) {
	buf, err := ioutil.ReadFile("test/lexer/constsUpper.pl0")
	if err != nil {
		t.Errorf("Error open file test/lexer/constsUpper.pl0")
	}

	table := []struct {
		iType ItemType
		iVal  string
	}{
		{ItemConst, "coNSt"},
		{ItemIdentifier, "gAGa22aa"},
		{ItemEqual, "="},
		{ItemNumber, "10"},
		{ItemComma, ","},
		{ItemIdentifier, "soNoNo25"},
		{ItemEqual, "="},
		{ItemNumber, "25"},
		{ItemSemicolon, ";"},
		{ItemEOF, ""},
	}

	var wg sync.WaitGroup
	wg.Add(1)

	i := 0
	lexer := Lex("constsUpper.pl0", string(buf), &wg)
	for item := range lexer.items {
		assert.Equal(t, table[i].iType, item.typ)
		assert.Equal(t, table[i].iVal, item.val)
		i++
	}
}

func TestLexVar(t *testing.T) {
	buf, err := ioutil.ReadFile("test/lexer/var.pl0")
	if err != nil {
		t.Errorf("Error open file test/lexer/var.pl0")
	}

	table := []struct {
		iType ItemType
		iVal  string
	}{
		{ItemVar, "var"},
		{ItemIdentifier, "sinagoga"},
		{ItemSemicolon, ";"},
		{ItemEOF, ""},
	}

	var wg sync.WaitGroup
	wg.Add(1)

	i := 0
	lexer := Lex("var.pl0", string(buf), &wg)
	for item := range lexer.items {
		assert.Equal(t, table[i].iType, item.typ)
		assert.Equal(t, table[i].iVal, item.val)
		i++
	}
}

func TestLexVarsUpperPlusNumbers(t *testing.T) {
	buf, err := ioutil.ReadFile("test/lexer/vars.pl0")
	if err != nil {
		t.Errorf("Error open file test/lexer/vars.pl0")
	}

	table := []struct {
		iType ItemType
		iVal  string
	}{
		{ItemVar, "vAr"},
		{ItemIdentifier, "ZiZou"},
		{ItemComma, ","},
		{ItemIdentifier, "Long23Dot"},
		{ItemComma, ","},
		{ItemIdentifier, "ZarZis"},
		{ItemSemicolon, ";"},
		{ItemEOF, ""},
	}

	var wg sync.WaitGroup
	wg.Add(1)

	i := 0
	lexer := Lex("vars.pl0", string(buf), &wg)
	for item := range lexer.items {
		assert.Equal(t, table[i].iType, item.typ)
		assert.Equal(t, table[i].iVal, item.val)
		i++
	}
}

func TestLexProcedure(t *testing.T) {
	buf, err := ioutil.ReadFile("test/lexer/procedure.pl0")
	if err != nil {
		t.Errorf("Error open file test/lexer/procedure.pl0")
	}

	table := []struct {
		iType ItemType
		iVal  string
	}{
		{ItemProcedure, "procedure"},
		{ItemIdentifier, "sarasa"},
		{ItemSemicolon, ";"},
		{ItemEOF, ""},
	}

	var wg sync.WaitGroup
	wg.Add(1)

	i := 0
	lexer := Lex("procedure.pl0", string(buf), &wg)
	for item := range lexer.items {
		assert.Equal(t, table[i].iType, item.typ)
		assert.Equal(t, table[i].iVal, item.val)
		i++
	}
}

func TestLexProcedurePlusBlock(t *testing.T) {
	buf, err := ioutil.ReadFile("test/lexer/procedureAndBlock.pl0")
	if err != nil {
		t.Errorf("Error open file test/lexer/procedureAndBlock.pl0")
	}

	table := []struct {
		iType ItemType
		iVal  string
		iLine int
		iCol  int
	}{
		{ItemProcedure, "procedure", 1, 0},
		{ItemIdentifier, "sarasa", 1, 10},
		{ItemSemicolon, ";", 1, 16},
		{ItemConst, "const", 2, 10},
		{ItemIdentifier, "poli22", 2, 16},
		{ItemEqual, "=", 2, 23},
		{ItemNumber, "21349", 2, 25},
		{ItemComma, ",", 2, 30},
		{ItemIdentifier, "Lala99", 2, 32},
		{ItemEqual, "=", 2, 39},
		{ItemMinus, "-", 2, 41},
		{ItemNumber, "1", 2, 42},
		{ItemSemicolon, ";", 2, 43},
		{ItemVar, "var", 3, 10},
		{ItemIdentifier, "Fundamental", 3, 14},
		{ItemComma, ",", 3, 25},
		{ItemIdentifier, "Ivy33", 3, 27},
		{ItemSemicolon, ";", 3, 32},
		{ItemEOF, "", 3, 33},
	}

	var wg sync.WaitGroup
	wg.Add(1)

	i := 0
	lexer := Lex("procedureAndBlock.pl0", string(buf), &wg)
	for item := range lexer.items {
		assert.Equal(t, table[i].iType, item.typ)
		assert.Equal(t, table[i].iVal, item.val)
		assert.Equal(t, table[i].iLine, item.line)
		assert.Equal(t, table[i].iCol, item.col)
		i++
	}
}

func TestLexPropositionAssign(t *testing.T) {
	buf, err := ioutil.ReadFile("test/lexer/propositionAssign.pl0")
	if err != nil {
		t.Errorf("Error open file test/lexer/propositionAssign.pl0")
	}

	table := []struct {
		iType ItemType
		iVal  string
		iLine int
		iCol  int
	}{
		{ItemIdentifier, "identify23", 1, 0},
		{ItemAssign, ":=", 1, 11},
		{ItemMinus, "-", 1, 14},
		{ItemNumber, "32", 1, 15},
		{ItemSemicolon, ";", 1, 17},
		{ItemIdentifier, "secondKing", 2, 0},
		{ItemAssign, ":=", 2, 11},
		{ItemIdentifier, "firstKing1", 2, 14},
		{ItemSemicolon, ";", 2, 24},
		{ItemIdentifier, "Gualy", 3, 0},
		{ItemAssign, ":=", 3, 6},
		{ItemMinus, "-", 3, 9},
		{ItemIdentifier, "resting33", 3, 10},
		{ItemSemicolon, ";", 3, 19},
		{ItemIdentifier, "rubik100", 4, 0},
		{ItemAssign, ":=", 4, 9},
		{ItemPlus, "+", 4, 12},
		{ItemIdentifier, "secondKing", 4, 13},
		{ItemPlus, "+", 4, 24},
		{ItemNumber, "33", 4, 26},
		{ItemSemicolon, ";", 4, 28},
		{ItemIdentifier, "finisterr", 5, 0},
		{ItemAssign, ":=", 5, 10},
		{ItemMinus, "-", 5, 13},
		{ItemIdentifier, "restInPeace", 5, 14},
		{ItemMinus, "-", 5, 26},
		{ItemNumber, "23", 5, 28},
		{ItemMinus, "-", 5, 31},
		{ItemIdentifier, "lala90", 5, 33},
		{ItemSemicolon, ";", 5, 39},
		{ItemEOF, "", 5, 40},
	}

	var wg sync.WaitGroup
	wg.Add(1)

	i := 0
	lexer := Lex("propositionAssign.pl0", string(buf), &wg)
	for item := range lexer.items {
		assert.Equal(t, table[i].iType, item.typ)
		assert.Equal(t, table[i].iVal, item.val)
		assert.Equal(t, table[i].iLine, item.line)
		assert.Equal(t, table[i].iCol, item.col)
		i++
	}
}

func TestLexExpressions(t *testing.T) {
	buf, err := ioutil.ReadFile("test/lexer/expressions.pl0")
	if err != nil {
		t.Errorf("Error open file test/lexer/expressions.pl0")
	}

	table := []struct {
		iType ItemType
		iVal  string
		iLine int
		iCol  int
	}{
		{ItemIdentifier, "lala", 1, 0},
		{ItemAssign, ":=", 1, 5},
		{ItemNumber, "34", 1, 8},
		{ItemTimes, "*", 1, 11},
		{ItemNumber, "33", 1, 13},
		{ItemSemicolon, ";", 1, 15},
		{ItemIdentifier, "lolo09", 2, 0},
		{ItemAssign, ":=", 2, 7},
		{ItemMinus, "-", 2, 10},
		{ItemNumber, "99", 2, 11},
		{ItemDivide, "/", 2, 14},
		{ItemNumber, "25", 2, 16},
		{ItemSemicolon, ";", 2, 18},
		{ItemIdentifier, "senesi", 3, 0},
		{ItemAssign, ":=", 3, 7},
		{ItemIdentifier, "deoro", 3, 10},
		{ItemTimes, "*", 3, 16},
		{ItemOpenParen, "(", 3, 18},
		{ItemIdentifier, "trail", 3, 19},
		{ItemDivide, "/", 3, 25},
		{ItemNumber, "2345", 3, 27},
		{ItemCloseParen, ")", 3, 31},
		{ItemSemicolon, ";", 3, 32},
		{ItemIdentifier, "ultima2", 4, 0},
		{ItemAssign, ":=", 4, 8},
		{ItemMinus, "-", 4, 11},
		{ItemIdentifier, "lolo", 4, 12},
		{ItemPlus, "+", 4, 17},
		{ItemOpenParen, "(", 4, 19},
		{ItemIdentifier, "dEde", 4, 20},
		{ItemPlus, "+", 4, 25},
		{ItemIdentifier, "fIf2", 4, 27},
		{ItemDivide, "/", 4, 32},
		{ItemNumber, "234", 4, 34},
		{ItemCloseParen, ")", 4, 37},
		{ItemMinus, "-", 4, 39},
		{ItemIdentifier, "asereje23", 4, 41},
		{ItemSemicolon, ";", 4, 50},
		{ItemIdentifier, "lastOne", 5, 0},
		{ItemAssign, ":=", 5, 8},
		{ItemIdentifier, "lola", 5, 11},
		{ItemDivide, "/", 5, 16},
		{ItemOpenParen, "(", 5, 18},
		{ItemIdentifier, "dede", 5, 19},
		{ItemMinus, "-", 5, 24},
		{ItemNumber, "1234", 5, 26},
		{ItemCloseParen, ")", 5, 30},
		{ItemTimes, "*", 5, 32},
		{ItemIdentifier, "asensio", 5, 34},
		{ItemSemicolon, ";", 5, 41},
		{ItemEOF, "", 5, 42},
	}

	var wg sync.WaitGroup
	wg.Add(1)

	i := 0
	lexer := Lex("expressions.pl0", string(buf), &wg)
	for item := range lexer.items {
		assert.Equal(t, table[i].iType, item.typ)
		assert.Equal(t, table[i].iVal, item.val)
		assert.Equal(t, table[i].iLine, item.line)
		assert.Equal(t, table[i].iCol, item.col)
		i++
	}
}

func TestLexCall(t *testing.T) {
	buf, err := ioutil.ReadFile("test/lexer/call.pl0")
	if err != nil {
		t.Errorf("Error open file test/lexer/call.pl0")
	}

	table := []struct {
		iType ItemType
		iVal  string
		iLine int
	}{
		{ItemCall, "call", 1},
		{ItemIdentifier, "something", 1},
		{ItemDot, ".", 1},
		{ItemEOF, "", 1},
	}

	var wg sync.WaitGroup
	wg.Add(1)

	i := 0
	lexer := Lex("call.pl0", string(buf), &wg)
	for item := range lexer.items {
		assert.Equal(t, table[i].iType, item.typ)
		assert.Equal(t, table[i].iVal, item.val)
		assert.Equal(t, table[i].iLine, item.line)
		i++
	}
}

func TestLexBegin(t *testing.T) {
	buf, err := ioutil.ReadFile("test/lexer/begin.pl0")
	if err != nil {
		t.Errorf("Error open file test/lexer/begin.pl0")
	}

	table := []struct {
		iType ItemType
		iVal  string
		iLine int
	}{
		{ItemBegin, "begin", 1},
		{ItemCall, "call", 2},
		{ItemIdentifier, "pablo", 2},
		{ItemSemicolon, ";", 2},
		{ItemIdentifier, "zathura", 3},
		{ItemAssign, ":=", 3},
		{ItemOpenParen, "(", 3},
		{ItemNumber, "33", 3},
		{ItemMinus, "-", 3},
		{ItemIdentifier, "zothos", 3},
		{ItemCloseParen, ")", 3},
		{ItemTimes, "*", 3},
		{ItemNumber, "2349087", 3},
		{ItemEnd, "end", 4},
		{ItemDot, ".", 4},
		{ItemEOF, "", 4},
	}

	var wg sync.WaitGroup
	wg.Add(1)

	i := 0
	lexer := Lex("begin.pl0", string(buf), &wg)
	for item := range lexer.items {
		assert.Equal(t, table[i].iType, item.typ, "types must be equal")
		assert.Equal(t, table[i].iVal, item.val)
		assert.Equal(t, table[i].iLine, item.line, "lines must be equal")
		i++
	}
}

func TestLexIfThen(t *testing.T) {
	buf, err := ioutil.ReadFile("test/lexer/if.pl0")
	if err != nil {
		t.Errorf("Error open file test/lexer/if.pl0")
	}

	table := []struct {
		iType ItemType
		iVal  string
		iLine int
		iCol  int
	}{
		{ItemBegin, "begin", 1, 0},
		{ItemIf, "if", 2, 8},
		{ItemIdentifier, "a", 2, 11},
		{ItemGreaterThan, ">", 2, 13},
		{ItemIdentifier, "b", 2, 15},
		{ItemThen, "then", 2, 17},
		{ItemCall, "call", 3, 11},
		{ItemIdentifier, "benja", 3, 16},
		{ItemIf, "if", 4, 8},
		{ItemNumber, "3", 4, 11},
		{ItemLessThan, "<", 4, 13},
		{ItemNumber, "90", 4, 15},
		{ItemThen, "tHEn", 4, 18},
		{ItemIdentifier, "founded", 5, 11},
		{ItemAssign, ":=", 5, 19},
		{ItemIdentifier, "true", 5, 22},
		{ItemIf, "IF", 6, 8},
		{ItemIdentifier, "benja", 6, 11},
		{ItemEqual, "=", 6, 17},
		{ItemNumber, "234", 6, 19},
		{ItemThen, "then", 6, 23},
		{ItemIdentifier, "benja", 7, 11},
		{ItemAssign, ":=", 7, 17},
		{ItemIdentifier, "benja", 7, 20},
		{ItemPlus, "+", 7, 26},
		{ItemNumber, "1", 7, 28},
		{ItemIf, "if", 8, 8},
		{ItemIdentifier, "gualy23", 8, 11},
		{ItemDifferent, "<>", 8, 19},
		{ItemIdentifier, "benja", 8, 22},
		{ItemThen, "then", 8, 28},
		{ItemIdentifier, "gualy23", 9, 11},
		{ItemAssign, ":=", 9, 19},
		{ItemIdentifier, "benja", 9, 22},
		{ItemMinus, "-", 9, 28},
		{ItemIdentifier, "gualy23", 9, 30},
		{ItemIf, "If", 10, 8},
		{ItemNumber, "1234", 10, 11},
		{ItemGreaterThanOrEqual, ">=", 10, 16},
		{ItemNumber, "9876", 10, 19},
		{ItemTimes, "*", 10, 24},
		{ItemNumber, "2", 10, 26},
		{ItemThen, "then", 10, 28},
		{ItemIdentifier, "count", 11, 11},
		{ItemAssign, ":=", 11, 17},
		{ItemIdentifier, "count", 11, 20},
		{ItemPlus, "+", 11, 26},
		{ItemNumber, "1", 11, 28},
		{ItemIf, "iF", 12, 8},
		{ItemNumber, "3", 12, 11},
		{ItemLessThanOrEqual, "<=", 12, 13},
		{ItemNumber, "4", 12, 16},
		{ItemDivide, "/", 12, 18},
		{ItemNumber, "3", 12, 20},
		{ItemThen, "then", 12, 22},
		{ItemIdentifier, "gualy", 13, 11},
		{ItemAssign, ":=", 13, 17},
		{ItemNumber, "3", 13, 20},
		{ItemTimes, "*", 13, 22},
		{ItemOpenParen, "(", 13, 24},
		{ItemNumber, "3", 13, 25},
		{ItemDivide, "/", 13, 27},
		{ItemNumber, "2", 13, 29},
		{ItemCloseParen, ")", 13, 30},
		{ItemIf, "if", 14, 8},
		{ItemOdd, "odd", 14, 11},
		{ItemIdentifier, "gualy", 14, 15},
		{ItemThen, "then", 14, 21},
		{ItemIdentifier, "gualy", 15, 11},
		{ItemAssign, ":=", 15, 17},
		{ItemNumber, "0", 15, 20},
		{ItemEnd, "eNd", 16, 0},
		{ItemDot, ".", 16, 3},
		{ItemEOF, "", 16, 4},
	}

	var wg sync.WaitGroup
	wg.Add(1)

	i := 0
	lexer := Lex("if.pl0", string(buf), &wg)
	for item := range lexer.items {
		assert.Equal(t, table[i].iType, item.typ)
		assert.Equal(t, table[i].iVal, item.val)
		assert.Equal(t, table[i].iLine, item.line)
		assert.Equal(t, table[i].iCol, item.col, "col must be equal", item.val, item.line)
		i++
	}
}

func TestLexWhileDo(t *testing.T) {
	buf, err := ioutil.ReadFile("test/lexer/while.pl0")
	if err != nil {
		t.Errorf("Error open file test/lexer/while.pl0")
	}

	table := []struct {
		iType ItemType
		iVal  string
		iLine int
		iCol  int
	}{
		{ItemWhile, "while", 1, 0},
		{ItemIdentifier, "b", 1, 6},
		{ItemGreaterThan, ">", 1, 8},
		{ItemIdentifier, "a", 1, 10},
		{ItemDo, "do", 1, 12},
		{ItemBegin, "begin", 2, 3},
		{ItemIdentifier, "b", 3, 7},
		{ItemAssign, ":=", 3, 9},
		{ItemIdentifier, "b", 3, 12},
		{ItemPlus, "+", 3, 14},
		{ItemNumber, "1", 3, 16},
		{ItemSemicolon, ";", 3, 17},
		{ItemIdentifier, "a", 4, 7},
		{ItemAssign, ":=", 4, 9},
		{ItemIdentifier, "b", 4, 12},
		{ItemPlus, "+", 4, 14},
		{ItemNumber, "1", 4, 16},
		{ItemEnd, "end", 5, 3},
		{ItemDot, ".", 5, 6},
		{ItemEOF, "", 5, 7},
	}

	var wg sync.WaitGroup
	wg.Add(1)

	i := 0
	lexer := Lex("while.pl0", string(buf), &wg)
	for item := range lexer.items {
		assert.Equal(t, table[i].iType, item.typ)
		assert.Equal(t, table[i].iVal, item.val)
		assert.Equal(t, table[i].iLine, item.line)
		assert.Equal(t, table[i].iCol, item.col)
		i++
	}
}

func TestLexReadLn(t *testing.T) {
	buf, err := ioutil.ReadFile("test/lexer/readln.pl0")
	if err != nil {
		t.Errorf("Error open file test/lexer/readln.pl0")
	}

	table := []struct {
		iType ItemType
		iVal  string
		iLine int
	}{
		{ItemBegin, "begin", 1},
		{ItemReadln, "readln", 2},
		{ItemOpenParen, "(", 2},
		{ItemIdentifier, "benja", 2},
		{ItemCloseParen, ")", 2},
		{ItemSemicolon, ";", 2},
		{ItemReadln, "readln", 3},
		{ItemOpenParen, "(", 3},
		{ItemIdentifier, "benja", 3},
		{ItemComma, ",", 3},
		{ItemIdentifier, "charata", 3},
		{ItemComma, ",", 3},
		{ItemIdentifier, "wisconsin", 3},
		{ItemCloseParen, ")", 3},
		{ItemEnd, "end", 4},
		{ItemDot, ".", 4},
		{ItemEOF, "", 4},
	}

	var wg sync.WaitGroup
	wg.Add(1)

	i := 0
	lexer := Lex("readln.pl0", string(buf), &wg)
	for item := range lexer.items {
		assert.Equal(t, table[i].iType, item.typ)
		assert.Equal(t, table[i].iVal, item.val)
		assert.Equal(t, table[i].iLine, item.line)
		i++
	}
}

func TestLexWrites(t *testing.T) {
	buf, err := ioutil.ReadFile("test/lexer/writeln.pl0")
	if err != nil {
		t.Errorf("Error open file test/lexer/writeln.pl0")
	}

	table := []struct {
		iType ItemType
		iVal  string
		iLine int
	}{
		{ItemBegin, "begin", 1},
		{ItemWriteln, "writeln", 2},
		{ItemOpenParen, "(", 2},
		{ItemString, "\"esto es una hermosa cadena\"", 2},
		{ItemCloseParen, ")", 2},
		{ItemSemicolon, ";", 2},
		{ItemWrite, "write", 3},
		{ItemOpenParen, "(", 3},
		{ItemString, "\"Esto seria otra hermosa cadena\"", 3},
		{ItemCloseParen, ")", 3},
		{ItemSemicolon, ";", 3},
		{ItemWriteln, "writeln", 4},
		{ItemOpenParen, "(", 4},
		{ItemString, "'esto tambien es una cadena'", 4},
		{ItemCloseParen, ")", 4},
		{ItemEnd, "end", 5},
		{ItemDot, ".", 5},
		{ItemEOF, "", 5},
	}

	var wg sync.WaitGroup
	wg.Add(1)

	i := 0
	lexer := Lex("writeln.pl0", string(buf), &wg)
	for item := range lexer.items {
		assert.Equal(t, table[i].iType, item.typ)
		assert.Equal(t, table[i].iVal, item.val)
		assert.Equal(t, table[i].iLine, item.line)
		i++
	}
}

func TestLexBien06(t *testing.T) {
	buf, err := ioutil.ReadFile("test/lexer/bien06.pl0")
	if err != nil {
		t.Errorf("Error open file test/lexer/bien06.pl0")
	}

	table := []struct {
		iType ItemType
		iVal  string
		iLine int
		iCol  int
	}{
		{ItemVar, "var", 1, 0},
		{ItemIdentifier, "IMPORTE", 1, 4},
		{ItemComma, ",", 1, 11},
		{ItemIdentifier, "BILLETE", 1, 13},
		{ItemComma, ",", 1, 20},
		{ItemIdentifier, "VUELTO", 1, 22},
		{ItemComma, ",", 1, 28},
		{ItemIdentifier, "PESOS", 1, 30},
		{ItemComma, ",", 1, 35},
		{ItemIdentifier, "CENTAVOS", 1, 37},
		{ItemComma, ",", 1, 45},
		{ItemIdentifier, "VUELTOPESOS", 1, 47},
		{ItemComma, ",", 1, 58},
		{ItemIdentifier, "VUELTOCENTAVOS", 1, 60},
		{ItemSemicolon, ";", 1, 74},
		{ItemProcedure, "procedure", 4, 0},
		{ItemIdentifier, "ERROR", 4, 10},
		{ItemSemicolon, ";", 4, 15},
		{ItemBegin, "begin", 5, 0},
		{ItemWriteln, "writeln", 6, 2},
		{ItemOpenParen, "(", 6, 10},
		{ItemString, "'VALOR FUERA DE RANGO!'", 6, 11},
		{ItemCloseParen, ")", 6, 34},
		{ItemEnd, "end", 7, 0},
		{ItemSemicolon, ";", 7, 3},
		{ItemBegin, "begin", 9, 0},
		{ItemWriteln, "WRITELN", 10, 2},
		{ItemOpenParen, "(", 10, 10},
		{ItemString, "'*******************************************'", 10, 11},
		{ItemCloseParen, ")", 10, 56},
		{ItemSemicolon, ";", 10, 57},
		{ItemWriteln, "writeLn", 11, 2},
		{ItemOpenParen, "(", 11, 10},
		{ItemString, "'VUELTO PARA IMPORTES PAGADOS CON UN BILLETE'", 11, 11},
		{ItemCloseParen, ")", 11, 56},
		{ItemSemicolon, ";", 11, 57},
		{ItemWriteln, "WRITELN", 12, 2},
		{ItemOpenParen, "(", 12, 10},
		{ItemString, "'*******************************************'", 12, 11},
		{ItemCloseParen, ")", 12, 56},
		{ItemSemicolon, ";", 12, 57},
		{ItemWriteln, "writeln", 13, 2},
		{ItemSemicolon, ";", 13, 9},
		{ItemIdentifier, "IMPORTEOK", 14, 2},
		{ItemAssign, ":=", 14, 12},
		{ItemMinus, "-", 14, 15},
		{ItemNumber, "1", 14, 16},
		{ItemSemicolon, ";", 14, 17},
		{ItemWhile, "while", 15, 2},
		{ItemIdentifier, "IMPORTEOK", 15, 8},
		{ItemDifferent, "<>", 15, 18},
		{ItemNumber, "0", 15, 21},
		{ItemDo, "do", 15, 23},
		{ItemBegin, "begin", 16, 4},
		{ItemWriteln, "writeln", 17, 6},
		{ItemOpenParen, "(", 17, 14},
		{ItemString, "'IMPORTE (min $0.01 y max $100.00)'", 17, 15},
		{ItemCloseParen, ")", 17, 50},
		{ItemSemicolon, ";", 17, 51},
		{ItemIdentifier, "CENTAVOSOK", 18, 6},
		{ItemAssign, ":=", 18, 17},
		{ItemMinus, "-", 18, 20},
		{ItemNumber, "1", 18, 21},
		{ItemSemicolon, ";", 18, 22},
		{ItemWhile, "while", 19, 6},
		{ItemIdentifier, "CENTAVOSOK", 19, 12},
		{ItemDifferent, "<>", 19, 23},
		{ItemNumber, "0", 19, 26},
		{ItemDo, "do", 19, 28},
		{ItemBegin, "begin", 20, 8},
		{ItemWrite, "write", 21, 10},
		{ItemOpenParen, "(", 21, 16},
		{ItemString, "'CENTAVOS: '", 21, 17},
		{ItemCloseParen, ")", 21, 29},
		{ItemSemicolon, ";", 21, 30},
		{ItemReadln, "readLn", 21, 32},
		{ItemOpenParen, "(", 21, 39},
		{ItemIdentifier, "CENTAVOS", 21, 40},
		{ItemCloseParen, ")", 21, 48},
		{ItemSemicolon, ";", 21, 49},
		{ItemIdentifier, "CENTAVOSOK", 22, 10},
		{ItemAssign, ":=", 22, 21},
		{ItemNumber, "0", 22, 24},
		{ItemSemicolon, ";", 22, 25},
		{ItemIf, "if", 23, 10},
		{ItemIdentifier, "CENTAVOS", 23, 13},
		{ItemLessThan, "<", 23, 22},
		{ItemNumber, "0", 23, 24},
		{ItemThen, "then", 23, 26},
		{ItemIdentifier, "CENTAVOSOK", 23, 31},
		{ItemAssign, ":=", 23, 42},
		{ItemMinus, "-", 23, 45},
		{ItemNumber, "1", 23, 46},
		{ItemSemicolon, ";", 23, 47},
		{ItemIf, "if", 24, 10},
		{ItemIdentifier, "CENTAVOS", 24, 13},
		{ItemGreaterThan, ">", 24, 22},
		{ItemNumber, "99", 24, 24},
		{ItemThen, "then", 24, 27},
		{ItemIdentifier, "CENTAVOSOK", 24, 32},
		{ItemAssign, ":=", 24, 43},
		{ItemMinus, "-", 24, 46},
		{ItemNumber, "1", 24, 47},
		{ItemSemicolon, ";", 24, 48},
		{ItemIf, "if", 25, 10},
		{ItemIdentifier, "CENTAVOSOK", 25, 13},
		{ItemDifferent, "<>", 25, 24},
		{ItemNumber, "0", 25, 27},
		{ItemThen, "then", 25, 29},
		{ItemCall, "call", 25, 34},
		{ItemIdentifier, "ERROR", 25, 39},
		{ItemEnd, "end", 26, 8},
		{ItemSemicolon, ";", 26, 11},
		{ItemIdentifier, "PESOSOK", 27, 6},
		{ItemAssign, ":=", 27, 14},
		{ItemMinus, "-", 27, 17},
		{ItemNumber, "1", 27, 18},
		{ItemSemicolon, ";", 27, 19},
		{ItemWhile, "while", 28, 6},
		{ItemIdentifier, "PESOSOK", 28, 12},
		{ItemDifferent, "<>", 28, 20},
		{ItemNumber, "0", 28, 23},
		{ItemDo, "do", 28, 25},
		{ItemBegin, "begin", 29, 8},
		{ItemWrite, "write", 30, 10},
		{ItemOpenParen, "(", 30, 16},
		{ItemString, "'PESOS: '", 30, 17},
		{ItemCloseParen, ")", 30, 26},
		{ItemSemicolon, ";", 30, 27},
		{ItemReadln, "readLn", 30, 29},
		{ItemOpenParen, "(", 30, 36},
		{ItemIdentifier, "PESOS", 30, 37},
		{ItemCloseParen, ")", 30, 42},
		{ItemSemicolon, ";", 30, 43},
		{ItemIdentifier, "PESOSOK", 31, 10},
		{ItemAssign, ":=", 31, 18},
		{ItemNumber, "0", 31, 21},
		{ItemSemicolon, ";", 31, 22},
		{ItemIf, "if", 32, 10},
		{ItemIdentifier, "PESOS", 32, 13},
		{ItemLessThan, "<", 32, 19},
		{ItemNumber, "0", 32, 21},
		{ItemThen, "then", 32, 23},
		{ItemIdentifier, "PESOSOK", 32, 28},
		{ItemAssign, ":=", 32, 36},
		{ItemMinus, "-", 32, 39},
		{ItemNumber, "1", 32, 40},
		{ItemSemicolon, ";", 32, 41},
		{ItemIf, "if", 33, 10},
		{ItemIdentifier, "PESOS", 33, 13},
		{ItemGreaterThan, ">", 33, 19},
		{ItemNumber, "100", 33, 21},
		{ItemThen, "then", 33, 25},
		{ItemIdentifier, "PESOSOK", 33, 30},
		{ItemAssign, ":=", 33, 38},
		{ItemMinus, "-", 33, 41},
		{ItemNumber, "1", 33, 42},
		{ItemSemicolon, ";", 33, 43},
		{ItemIf, "if", 34, 10},
		{ItemIdentifier, "PESOSOK", 34, 13},
		{ItemDifferent, "<>", 34, 21},
		{ItemNumber, "0", 34, 24},
		{ItemThen, "then", 34, 26},
		{ItemCall, "call", 34, 31},
		{ItemIdentifier, "ERROR", 34, 36},
		{ItemEnd, "end", 35, 7},
		{ItemSemicolon, ";", 35, 10},
		{ItemWrite, "write", 36, 6},
		{ItemOpenParen, "(", 36, 12},
		{ItemString, "'IMPORTE: $'", 36, 13},
		{ItemComma, ",", 36, 25},
		{ItemIdentifier, "PESOS", 36, 27},
		{ItemComma, ",", 36, 32},
		{ItemString, "'.'", 36, 34},
		{ItemCloseParen, ")", 36, 37},
		{ItemSemicolon, ";", 36, 38},
		{ItemIf, "if", 37, 6},
		{ItemIdentifier, "CENTAVOS", 37, 9},
		{ItemLessThan, "<", 37, 18},
		{ItemNumber, "10", 37, 20},
		{ItemThen, "then", 37, 23},
		{ItemWrite, "write", 37, 28},
		{ItemOpenParen, "(", 37, 34},
		{ItemString, "'0'", 37, 35},
		{ItemCloseParen, ")", 37, 38},
		{ItemSemicolon, ";", 37, 39},
		{ItemWriteln, "writeLn", 38, 6},
		{ItemOpenParen, "(", 38, 14},
		{ItemIdentifier, "CENTAVOS", 38, 15},
		{ItemCloseParen, ")", 38, 23},
		{ItemSemicolon, ";", 38, 24},
		{ItemIdentifier, "IMPORTE", 39, 6},
		{ItemAssign, ":=", 39, 14},
		{ItemIdentifier, "PESOS", 39, 17},
		{ItemTimes, "*", 39, 23},
		{ItemNumber, "100", 39, 25},
		{ItemPlus, "+", 39, 29},
		{ItemIdentifier, "CENTAVOS", 39, 31},
		{ItemSemicolon, ";", 39, 39},
		{ItemIdentifier, "IMPORTEOK", 40, 6},
		{ItemAssign, ":=", 40, 16},
		{ItemNumber, "0", 40, 19},
		{ItemSemicolon, ";", 40, 20},
		{ItemIf, "if", 41, 6},
		{ItemIdentifier, "IMPORTE", 41, 9},
		{ItemLessThan, "<", 41, 17},
		{ItemNumber, "1", 41, 19},
		{ItemThen, "then", 41, 21},
		{ItemIdentifier, "IMPORTEOK", 41, 26},
		{ItemAssign, ":=", 41, 36},
		{ItemMinus, "-", 41, 39},
		{ItemNumber, "1", 41, 40},
		{ItemSemicolon, ";", 41, 41},
		{ItemIf, "if", 42, 6},
		{ItemIdentifier, "IMPORTE", 42, 9},
		{ItemGreaterThan, ">", 42, 17},
		{ItemNumber, "10000", 42, 19},
		{ItemThen, "then", 42, 25},
		{ItemIdentifier, "IMPORTEOK", 42, 30},
		{ItemAssign, ":=", 42, 40},
		{ItemMinus, "-", 42, 43},
		{ItemNumber, "1", 42, 44},
		{ItemSemicolon, ";", 42, 45},
		{ItemIf, "if", 43, 6},
		{ItemIdentifier, "IMPORTEOK", 43, 9},
		{ItemDifferent, "<>", 43, 19},
		{ItemNumber, "0", 43, 22},
		{ItemThen, "then", 43, 24},
		{ItemCall, "call", 43, 29},
		{ItemIdentifier, "ERROR", 43, 34},
		{ItemEnd, "end", 44, 4},
		{ItemSemicolon, ";", 44, 7},
		{ItemEOF, "", 44, 8},
	}

	var wg sync.WaitGroup
	wg.Add(1)

	i := 0
	lexer := Lex("bien06.pl0", string(buf), &wg)
	for item := range lexer.items {
		assert.Equal(t, table[i].iType, item.typ, "types should be the same", table[i].iVal, table[i].iLine)
		assert.Equal(t, table[i].iVal, item.val, "values should be the same")
		assert.Equal(t, table[i].iLine, item.line, "the lines should be te same", item.line, table[i].iVal)
		assert.Equal(t, table[i].iCol, item.col)
		i++
	}
}
